package com.ideoholic.data.datalist;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ideoholic.data.dto.ShareClientData;
import com.ideoholic.utils.Constants;
import com.ideoholic.utils.excel.ExcelReader;
import com.ideoholic.utils.excel.ExcelReaderFactory;

public class ShareClientsList implements Iterator<ShareClientData>, Constants {
	private int MEMBER_NAME_POSTION = 0;
	private int MEMBER_GENDER_POSITION = 1;
	private int MEMBER_DOB_POSITION = 3;
	
	private Integer currentId, currentIterator;
	private Map<Integer, ShareClientData> clientMap;
	// Map of client ID and the number of times it is repeated
	private Map<Integer, Integer> duplicateShareClientDataMap;

	public ShareClientsList() throws FileNotFoundException, IOException {
		currentId = currentIterator = 0;
		clientMap = new HashMap<Integer, ShareClientData>();
		duplicateShareClientDataMap = new HashMap<Integer, Integer>();
		populateDataFromExcel();
	}

	private void populateDataFromExcel() throws FileNotFoundException, IOException {

		ExcelReader reader = ExcelReaderFactory.getExcelReader(MEMBER_DATA_FILE_XLSX, ExcelReaderFactory.TYPE.XLS);
		List<String> singleRow = reader.getNextRowData();
		while (singleRow != null && !singleRow.isEmpty()) {
			// System.out.println("ClientsList.populateDataFromExcel::Single Row:" +
			// singleRow);
			addClient(singleRow.get(TOTAL_SHARES_POSITION),singleRow.get(MEMBER_NAME_POSTION), singleRow.get(MEMBER_GENDER_POSITION),
					singleRow.get(MEMBER_DOB_POSITION));
			singleRow = reader.getNextRowData();
		}
	}

	public void addClient(String totalShares, String fullName, String gender, String dateOfBirth) {
		ShareClientData client = new ShareClientData(currentId, totalShares, fullName, gender, dateOfBirth);
		addClient(client);
		currentId += 1; // Increment for the purpose of the next client
	}

	public void addClient(ShareClientData client) {
		// If the client exists then increment count in the duplicate data map
		if (clientMap.containsValue(client)) {
			// fast-enumerating map's values
			for (ShareClientData mapClient : clientMap.values()) {
				if (client.equals(mapClient)) {
					Integer clientId = mapClient.getId();
					Integer count = duplicateShareClientDataMap.get(clientId);
					if (count == null) {
						count = 2;
					} else {
						count += 1;
					}
					duplicateShareClientDataMap.put(clientId, count);
				}
			}
		} else {
			clientMap.put(currentId, client);
		}
	}

	public void printAllClients() {
		for (ShareClientData client : clientMap.values())
			logger.info(client.getId() + ": " + client);
	}

	public void printDuplicateClients() {
		// using for-each loop for iteration over Map.entrySet()
		for (Map.Entry<Integer, Integer> entry : duplicateShareClientDataMap.entrySet())
			System.out.println("Duplicate count = " + entry.getValue() + ", Client = " + clientMap.get(entry.getKey()));
	}

	@Override
	public boolean hasNext() {
		if (currentIterator < currentId) {
			return true;
		}
		return false;
	}

	@Override
	public ShareClientData next() {
		ShareClientData data = clientMap.get(currentIterator);
		if (data == null) {
			logger.debug("ClientsList.next::Data is null for iteration:" + currentIterator);
		}
		currentIterator += 1;
		return data;
	}

	public static void main(String[] args) throws IOException {
		ShareClientsList list = new ShareClientsList();
		// list.printAllClients();
		list.printDuplicateClients();
		System.out.println("=================================================");
		/*while (list.hasNext()) {
			System.out.println("Client:" + list.next());
		}*/
	}

}
