package com.ideoholic.data.datalist;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ideoholic.data.dto.LoanData;
import com.ideoholic.utils.Constants;
import com.ideoholic.utils.excel.ExcelReader;
import com.ideoholic.utils.excel.ExcelReaderFactory;

public class LoanList implements Iterator<LoanData>, Constants {
	private Integer currentId, currentIterator;
	private Map<Integer, LoanData> loanMap;
	// Map of loan ID and the number of times it is repeated
	private Map<Integer, Integer> duplicateLoanDataMap;

	public LoanList(int typeOfLoanProduct) throws FileNotFoundException, IOException {
		currentId = currentIterator = 0;
		loanMap = new HashMap<Integer, LoanData>();
		duplicateLoanDataMap = new HashMap<Integer, Integer>();
		if (typeOfLoanProduct == 1) {
			populateDataFromGoldLoanExcel();
		} else if (typeOfLoanProduct == 2) {
			populateDataFromMicrofinanceLoanExcel();
		} else if (typeOfLoanProduct == 3) {
			populateDataFromStaffLoanExcel();
		} else if (typeOfLoanProduct == 5) {
			populateDataFromVehicleLoanExcel();
		}

	}

	private void populateDataFromGoldLoanExcel() throws FileNotFoundException, IOException {

		ExcelReader reader = ExcelReaderFactory.getExcelReader(GOLD_LOAN_CREATION_DATA_FILE,
				ExcelReaderFactory.TYPE.XLS);
		List<String> singleRow = reader.getNextRowData();

		while (singleRow != null && !singleRow.isEmpty()) {
			System.out.println("LoansList.populateDataFromExcel::Single Row:" + singleRow);

			addLoan(singleRow.get(LOAN_MEMBER_NAME_POSTION), singleRow.get(LOAN_PRODUCT_TYPE_POSITION),
					singleRow.get(LOAN_PRINCIPAL_AMOUNT_POSITION), singleRow.get(LOAN_TERM_POSITION),
					singleRow.get(CHARGE_AMOUNT_POSITION), singleRow.get(CHARGE_DUE_DATE_POSITION),
					singleRow.get(LOAN_DISBURSE_DATE_POSITION), singleRow.get(LOAN_SUBMITTED_DATE_POSITION));
			singleRow = reader.getNextRowData();
		}
	}

	private void populateDataFromMicrofinanceLoanExcel() throws FileNotFoundException, IOException {

		ExcelReader reader = ExcelReaderFactory.getExcelReader(MICROFINANCE_LOAN_CREATION_DATA_FILE,
				ExcelReaderFactory.TYPE.XLS);
		List<String> singleRow = reader.getNextRowData();

		while (singleRow != null && !singleRow.isEmpty()) {
			System.out.println("LoansList.populateDataFromExcel::Single Row:" + singleRow);

			addLoan(singleRow.get(LOAN_MEMBER_NAME_POSTION), singleRow.get(LOAN_PRODUCT_TYPE_POSITION),
					singleRow.get(LOAN_PRINCIPAL_AMOUNT_POSITION), singleRow.get(LOAN_TERM_POSITION),
					singleRow.get(CHARGE_AMOUNT_POSITION), singleRow.get(CHARGE_DUE_DATE_POSITION),
					singleRow.get(LOAN_DISBURSE_DATE_POSITION), singleRow.get(LOAN_SUBMITTED_DATE_POSITION));
			singleRow = reader.getNextRowData();
		}
	}

	private void populateDataFromStaffLoanExcel() throws FileNotFoundException, IOException {

		ExcelReader reader = ExcelReaderFactory.getExcelReader(STAFF_LOAN_CREATION_DATA_FILE,
				ExcelReaderFactory.TYPE.XLS);
		List<String> singleRow = reader.getNextRowData();

		while (singleRow != null && !singleRow.isEmpty()) {
			System.out.println("LoansList.populateDataFromExcel::Single Row:" + singleRow);

			addLoan(singleRow.get(LOAN_MEMBER_NAME_POSTION), singleRow.get(LOAN_PRODUCT_TYPE_POSITION),
					singleRow.get(LOAN_PRINCIPAL_AMOUNT_POSITION), singleRow.get(LOAN_TERM_POSITION),
					singleRow.get(CHARGE_AMOUNT_POSITION), singleRow.get(CHARGE_DUE_DATE_POSITION),
					singleRow.get(LOAN_DISBURSE_DATE_POSITION), singleRow.get(LOAN_SUBMITTED_DATE_POSITION));
			singleRow = reader.getNextRowData();
		}
	}

	private void populateDataFromVehicleLoanExcel() throws FileNotFoundException, IOException {

		ExcelReader reader = ExcelReaderFactory.getExcelReader(VEHICLE_LOAN_CREATION_DATA_FILE,
				ExcelReaderFactory.TYPE.XLS);
		List<String> singleRow = reader.getNextRowData();

		while (singleRow != null && !singleRow.isEmpty()) {
			System.out.println("LoansList.populateDataFromExcel::Single Row:" + singleRow);

			addLoan(singleRow.get(LOAN_MEMBER_NAME_POSTION), singleRow.get(LOAN_PRODUCT_TYPE_POSITION),
					singleRow.get(LOAN_PRINCIPAL_AMOUNT_POSITION), singleRow.get(LOAN_TERM_POSITION),
					singleRow.get(CHARGE_AMOUNT_POSITION), singleRow.get(CHARGE_DUE_DATE_POSITION),
					singleRow.get(LOAN_DISBURSE_DATE_POSITION), singleRow.get(LOAN_SUBMITTED_DATE_POSITION));
			singleRow = reader.getNextRowData();
		}
	}

	public void addLoan(String nameOfTheMember, String loanProductType, String loanPrincipalAmount, String loanTerm,
			String loanChargeAmount, String loanChargeDueOnDate, String loanDisburseDate, String loanSubmittedDate) {
		LoanData loan = new LoanData(currentId, nameOfTheMember, loanProductType, loanPrincipalAmount, loanTerm,
				loanChargeAmount, loanChargeDueOnDate, loanDisburseDate, loanSubmittedDate);
		addLoan(loan);
		currentId += 1; // Increment for the purpose of the next loan
	}

	private void addLoan(LoanData loan) {
		// If the loan exists then increment count in the duplicate data map
		if (loanMap.containsValue(loan)) {
			// fast-enumerating map's values
			for (LoanData mapLoan : loanMap.values()) {
				if (loan.equals(mapLoan)) {
					Integer loanId = mapLoan.getId();
					Integer count = duplicateLoanDataMap.get(loanId);
					if (count == null) {
						count = 2;
					} else {
						count += 1;
					}
					duplicateLoanDataMap.put(loanId, count);
				}
			}
		} else {
			loanMap.put(currentId, loan);
			System.out.println("LOAN NAME--:" + loan);

		}

	}

	public void printAllLoans() {
		for (LoanData loan : loanMap.values())
			logger.info(loan.getId() + ": " + loan);
	}

	public void printDuplicateLoans() {
		// using for-each loop for iteration over Map.entrySet()
		for (Map.Entry<Integer, Integer> entry : duplicateLoanDataMap.entrySet())
			System.out.println("Duplicate count = " + entry.getValue() + ", loan = " + loanMap.get(entry.getKey()));
	}

	@Override
	public boolean hasNext() {
		if (currentIterator < currentId) {
			return true;
		}
		return false;
	}

	@Override
	public LoanData next() {
		LoanData data = loanMap.get(currentIterator);
		if (data == null) {
			logger.debug("LoanList.next::Data is null for iteration:" + currentIterator);
		}
		currentIterator += 1;
		return data;
	}

}
