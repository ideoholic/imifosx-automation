package com.ideoholic.data.datalist;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ideoholic.data.dto.LoanRepaymentData;
import com.ideoholic.utils.Constants;
import com.ideoholic.utils.excel.ExcelReader;
import com.ideoholic.utils.excel.ExcelReaderFactory;

public class LoanRepaymentList implements Iterator<LoanRepaymentData>, Constants {
	private Integer currentId, currentIterator;
	private Map<Integer, LoanRepaymentData> loanRepaymentMap;
	// Map of client ID and the number of times it is repeated
	private Map<Integer, Integer> duplicateLoanRepaymentDataMap;

	public LoanRepaymentList() throws FileNotFoundException, IOException {
		currentId = currentIterator = 0;
		loanRepaymentMap = new HashMap<Integer, LoanRepaymentData>();
		duplicateLoanRepaymentDataMap = new HashMap<Integer, Integer>();
		populateDataFromExcel();
	}

	private void populateDataFromExcel() throws FileNotFoundException, IOException {

		ExcelReader reader = ExcelReaderFactory.getExcelReader(LOAN_TRANSACTION_DATA_FILE, ExcelReaderFactory.TYPE.XLS);
		List<String> singleRow = reader.getNextRowData();
		while (singleRow != null && !singleRow.isEmpty()) {
			 System.out.println("ClientsList.populateDataFromExcel::Single Row:" +
			 singleRow);

			addLoanRepayment(singleRow.get(LOAN_MEMBER_NAME_FOR_REPAYMENT_POSTION),singleRow.get(LOAN_ACCOUNT_NUMBER_POSITION),
					singleRow.get(TRANSACTION_DATE_POSITION), singleRow.get(LOAN_TRANSACTION_AMOUNT_POSITION) );
			singleRow = reader.getNextRowData();
		}
	}
	public void addLoanRepayment(String nameOfTheMemberForLoanTransaction, String loanAccountNumber,
			String loanTransactionDateForRepayment, String loanTransactionAmountForRepayment) {
		LoanRepaymentData loan = new LoanRepaymentData(currentId, nameOfTheMemberForLoanTransaction, loanAccountNumber,
				loanTransactionDateForRepayment, loanTransactionAmountForRepayment);
		addLoanRepayment(loan);
		currentId += 1; // Increment for the purpose of the next client
	}

	private void addLoanRepayment(LoanRepaymentData loan) {
			// If the client exists then increment count in the duplicate data map
					if (loanRepaymentMap.containsValue(loan)) {
						// fast-enumerating map's values
						for (LoanRepaymentData mapLoanReapyment : loanRepaymentMap.values()) {
							if (loan.equals(mapLoanReapyment)) {
								Integer clientId = mapLoanReapyment.getId();
								Integer count = duplicateLoanRepaymentDataMap.get(clientId);
								if (count == null) {
									count = 2;
								} else {
									count += 1;
								}
								duplicateLoanRepaymentDataMap.put(clientId, count);
							}
						}
					} else {
						loanRepaymentMap.put(currentId, loan);
					}
	}

	@Override
	public boolean hasNext() {
		if (currentIterator < currentId) {
			return true;
		}
		return false;
	}

	@Override
	public LoanRepaymentData next() {
		LoanRepaymentData data = loanRepaymentMap.get(currentIterator);
		if (data == null) {
			logger.debug("LoanRepaymentList.next::Data is null for iteration:" + currentIterator);
		}
		currentIterator += 1;
		return data;
	}

	public static void main(String[] args) throws IOException {
		LoanRepaymentList list = new LoanRepaymentList();
		// list.printAllClients();
		System.out.println("=================================================");
		/*
		 * while (list.hasNext()) { System.out.println("Client:" + list.next()); }
		 */
	}
}
