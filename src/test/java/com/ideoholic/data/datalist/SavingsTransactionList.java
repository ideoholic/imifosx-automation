package com.ideoholic.data.datalist;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ideoholic.data.dto.SavingsTransactionData;
import com.ideoholic.utils.Constants;
import com.ideoholic.utils.excel.ExcelReader;
import com.ideoholic.utils.excel.ExcelReaderFactory;

public class SavingsTransactionList implements Iterator<SavingsTransactionData>, Constants {
	private Integer currentId, currentIterator;
	private Map<Integer, SavingsTransactionData> clientMap;
	// Map of client ID and the number of times it is repeated
	private Map<Integer, Integer> duplicateClientDataMap;

	public SavingsTransactionList() throws FileNotFoundException, IOException {
		currentId = currentIterator = 0;
		clientMap = new HashMap<Integer, SavingsTransactionData>();
		duplicateClientDataMap = new HashMap<Integer, Integer>();
		populateDataFromExcel();
	}

	private void populateDataFromExcel() throws FileNotFoundException, IOException {

		ExcelReader reader = ExcelReaderFactory.getExcelReader(SAVINGS_DATA_FILE_XLSX, ExcelReaderFactory.TYPE.XLS);
		logger.debug("ClientsList.populateDataFromExcel::ExcelReader reader:" + reader);
		List<String> singleRow = reader.getNextRowData();
		while (singleRow != null && !singleRow.isEmpty()) {
			logger.debug("ClientsList.populateDataFromExcel::Single Row:" + singleRow);
			addClient(singleRow.get(SAVINGS_MEMBER_NAME_POSTION), singleRow.get(SAVINGS_ACCOUNT_NUMBER), singleRow.get(SAVINGS_TRANSACTION_DATE),
					singleRow.get(SAVINGS_TRANSACTION_AMOUNT), singleRow.get(SAVINGS_TRANSACTION_TYPE));
			singleRow = reader.getNextRowData();
		}
	}

	public void addClient(String clientFullName, String savingsAccountNumber, String transactionDate, String transactionAmount, String transactionType) {
		
		SavingsTransactionData client = new SavingsTransactionData(currentId, clientFullName, savingsAccountNumber, transactionDate, transactionAmount, transactionType);
		addClient(client);
		currentId += 1; // Increment for the purpose of the next client
	}

	public void addClient(SavingsTransactionData client) {
		// If the client exists then increment count in the duplicate data map
		if (clientMap.containsValue(client)) {
			// fast-enumerating map's values
			for (SavingsTransactionData mapClient : clientMap.values()) {
				if (client.equals(mapClient)) {
					Integer clientId = mapClient.getId();
					Integer count = duplicateClientDataMap.get(clientId);
					if (count == null) {
						count = 2;
					} else {
						count += 1;
					}
					duplicateClientDataMap.put(clientId, count);
				}
			}
		} else {
			clientMap.put(currentId, client);
		}
	}

	public void printAllClients() {
		for (SavingsTransactionData client : clientMap.values())
			logger.info(client.getId() + ": " + client);
	}

	public void printDuplicateClients() {
		// using for-each loop for iteration over Map.entrySet()
		for (Map.Entry<Integer, Integer> entry : duplicateClientDataMap.entrySet())
			System.out.println("Duplicate count = " + entry.getValue() + ", Client = " + clientMap.get(entry.getKey()));
	}

	@Override
	public boolean hasNext() {
		if (currentIterator < currentId) {
			return true;
		}
		return false;
	}

	@Override
	public SavingsTransactionData next() {
		SavingsTransactionData data = clientMap.get(currentIterator);
		if (data == null) {
			logger.debug("ClientsList.next::Data is null for iteration:" + currentIterator);
		}
		currentIterator += 1;
		return data;
	}

	public static void main(String[] args) throws IOException {
		SavingsTransactionList list = new SavingsTransactionList();
		// list.printAllClients();
		list.printDuplicateClients();
		System.out.println("=================================================");
		/*while (list.hasNext()) {
			System.out.println("Client:" + list.next());
		}*/
	}

}
