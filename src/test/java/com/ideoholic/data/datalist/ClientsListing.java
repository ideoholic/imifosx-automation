package com.ideoholic.data.datalist;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.ideoholic.data.dto.ClientData;
import com.ideoholic.utils.Constants;

public class ClientsListing implements Iterator<ClientData>, Constants {
	private Integer currentId, currentIterator;
	private Map<Integer, ClientData> clientMap;
	// Map of client ID and the number of times it is repeated
	private Map<Integer, Integer> duplicateClientDataMap;

	public ClientsListing(HashMap<Integer, ClientData> clientMap) {
		currentId = currentIterator = 0;
		this.clientMap = clientMap;
		duplicateClientDataMap = new HashMap<Integer, Integer>();
		addClients();
	}

	public void addClients() {
		for (Map.Entry<Integer, ClientData> client : clientMap.entrySet()) {
			addClient(client.getValue());
		}
		currentId += 1; // Increment for the purpose of the next client
	}

	public void addClient(ClientData client) {
		// If the client exists then increment count in the duplicate data map
		if (clientMap.containsValue(client)) {
			// fast-enumerating map's values
			for (ClientData mapClient : clientMap.values()) {
				if (client.equals(mapClient)) {
					Integer clientId = mapClient.getId();
					Integer count = duplicateClientDataMap.get(clientId);
					if (count == null) {
						count = 2;
					} else {
						count += 1;
					}
					duplicateClientDataMap.put(clientId, count);
				}
			}
		} else {
			clientMap.put(currentId, client);
		}
	}

	public void printAllClients() {
		for (ClientData client : clientMap.values())
			logger.info(client.getId() + ": " + client);
	}

	public void printDuplicateClients() {
		logger.info("List of duplicate clients");
		// using for-each loop for iteration over Map.entrySet()
		for (Map.Entry<Integer, Integer> entry : duplicateClientDataMap.entrySet())
			logger.info("Duplicate count = " + entry.getValue() + ", Client = " + clientMap.get(entry.getKey()));
	}

	@Override
	public boolean hasNext() {
		if (currentIterator < currentId) {
			return true;
		}
		return false;
	}

	@Override
	public ClientData next() {
		ClientData data = clientMap.get(currentIterator);
		if (data == null) {
			logger.debug("ClientsList.next::Data is null for iteration:" + currentIterator);
		}
		currentIterator += 1;
		return data;
	}

}
