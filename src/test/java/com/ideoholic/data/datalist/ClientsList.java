package com.ideoholic.data.datalist;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ideoholic.data.dto.ClientData;
import com.ideoholic.utils.Constants;
import com.ideoholic.utils.excel.ExcelReader;
import com.ideoholic.utils.excel.ExcelReaderFactory;

public class ClientsList implements Iterator<ClientData>, Constants {
	private Integer currentId, currentIterator;
	private Map<Integer, ClientData> clientMap;
	// Map of client ID and the number of times it is repeated
	private Map<Integer, Integer> duplicateClientDataMap;

	public ClientsList() throws FileNotFoundException, IOException {
		currentId = currentIterator = 0;
		clientMap = new HashMap<Integer, ClientData>();
		duplicateClientDataMap = new HashMap<Integer, Integer>();
		populateDataFromExcel();
	}

	private void populateDataFromExcel() throws FileNotFoundException, IOException {

		ExcelReader reader = ExcelReaderFactory.getExcelReader(MEMBER_DATA_FILE_XLSX, ExcelReaderFactory.TYPE.XLS);
		List<String> singleRow = reader.getNextRowData();
		while (singleRow != null && !singleRow.isEmpty()) {
			// System.out.println("ClientsList.populateDataFromExcel::Single Row:" +
			// singleRow);
			singleRow = reader.getNextRowData();
		}
	}

	public void addClient(String fullName, String gender, String dateOfBirth) {
		ClientData client = new ClientData();
		addClient(client);
		currentId += 1; // Increment for the purpose of the next client
	}

	public void addClient(ClientData client) {
		// If the client exists then increment count in the duplicate data map
		if (clientMap.containsValue(client)) {
			// fast-enumerating map's values
			for (ClientData mapClient : clientMap.values()) {
				if (client.equals(mapClient)) {
					Integer clientId = mapClient.getId();
					Integer count = duplicateClientDataMap.get(clientId);
					if (count == null) {
						count = 2;
					} else {
						count += 1;
					}
					duplicateClientDataMap.put(clientId, count);
				}
			}
		} else {
			clientMap.put(currentId, client);
		}
	}

	public void printAllClients() {
		for (ClientData client : clientMap.values())
			logger.info(client.getId() + ": " + client);
	}

	public void printDuplicateClients() {
		// using for-each loop for iteration over Map.entrySet()
		for (Map.Entry<Integer, Integer> entry : duplicateClientDataMap.entrySet())
			System.out.println("Duplicate count = " + entry.getValue() + ", Client = " + clientMap.get(entry.getKey()));
	}

	@Override
	public boolean hasNext() {
		if (currentIterator < currentId) {
			return true;
		}
		return false;
	}

	@Override
	public ClientData next() {
		ClientData data = clientMap.get(currentIterator);
		if (data == null) {
			logger.debug("ClientsList.next::Data is null for iteration:" + currentIterator);
		}
		currentIterator += 1;
		return data;
	}

	public static void main(String[] args) throws IOException {
		ClientsList list = new ClientsList();
		// list.printAllClients();
		list.printDuplicateClients();
		System.out.println("=================================================");
		/*
		 * while (list.hasNext()) { System.out.println("Client:" + list.next()); }
		 */
	}

}
