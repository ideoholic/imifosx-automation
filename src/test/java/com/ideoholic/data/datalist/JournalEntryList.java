package com.ideoholic.data.datalist;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.ideoholic.data.dto.JournalEntryData;
import com.ideoholic.utils.Constants;
import com.ideoholic.utils.excel.ExcelReader;
import com.ideoholic.utils.excel.ExcelReaderFactory;

public class JournalEntryList implements Iterator<JournalEntryData>, Constants{
	private Integer currentId, currentIterator;
	private Map<Integer, JournalEntryData> journalMap;
	// Map of client ID and the number of times it is repeated
	private Map<Integer, Integer> duplicateJournalDataMap;

	public JournalEntryList() throws FileNotFoundException, IOException {
		currentId = currentIterator = 0;
		journalMap = new HashMap<Integer, JournalEntryData>();
		duplicateJournalDataMap = new HashMap<Integer, Integer>();
		populateDataFromExcel();
	}
	
	private void populateDataFromExcel() throws FileNotFoundException, IOException {

		ExcelReader reader = ExcelReaderFactory.getExcelReader(JOURNAL_CREATION_DATA_FILE_XLSX, ExcelReaderFactory.TYPE.XLS);
		List<String> singleRow = reader.getNextRowData();
		while (singleRow != null && !singleRow.isEmpty()) {
			// System.out.println("ClientsList.populateDataFromExcel::Single Row:" +
			// singleRow);
			addJournal(singleRow.get(JOURNAL_DEBIT_ACCOUNT_NAME_POSITION), singleRow.get(JOURNAL_DEBIT_AMOUNT_POSITION),
					singleRow.get(JOURNAL_CREDIT_ACCOUNT_NAME_POSITION),singleRow.get(JOURNAL_CREDIT_AMOUNT_POSITION)
					,singleRow.get(JOURNAL_TRANSACTION_DATE_POSITION),singleRow.get(JOURNAL_COMMENTS_POSITION));
			singleRow = reader.getNextRowData();
		}
	}
	
	public void addJournal(String journalDebitAccountName, String journalDebitAmount, String journalCreditAccountName,
			String journalCreditAmount, String journalTransactionDate, String journalComments) {
		JournalEntryData journal = new JournalEntryData(currentId, journalDebitAccountName, journalDebitAmount, journalCreditAccountName,
				journalCreditAmount,journalTransactionDate,journalComments);
		addJournal(journal);
		currentId += 1; // Increment for the purpose of the next client
	}
	
	public void addJournal(JournalEntryData journal) {
		// If the client exists then increment count in the duplicate data map
		if (journalMap.containsValue(journal)) {
			// fast-enumerating map's values
			for (JournalEntryData mapJournal : journalMap.values()) {
				if (journal.equals(mapJournal)) {
					Integer journalId = mapJournal.getId();
					Integer count = duplicateJournalDataMap.get(journalId);
					if (count == null) {
						count = 2;
					} else {
						count += 1;
					}
					duplicateJournalDataMap.put(journalId, count);
				}
			}
		} else {
			journalMap.put(currentId, journal);
		}
	}
	public void printDuplicateJournal() {
		// using for-each loop for iteration over Map.entrySet()
		for (Map.Entry<Integer, Integer> entry : duplicateJournalDataMap.entrySet())
			System.out.println("Duplicate count = " + entry.getValue() + ", Journal = " + journalMap.get(entry.getKey()));
	}
	@Override
	public boolean hasNext() {
		if (currentIterator < currentId) {
			return true;
		}
		return false;
	}

	@Override
	public JournalEntryData next() {
		JournalEntryData data = journalMap.get(currentIterator);
		if (data == null) {
			logger.debug("ClientsList.next::Data is null for iteration:" + currentIterator);
		}
		currentIterator += 1;
		return data;
	}
	

	public static void main(String[] args) throws IOException {
		JournalEntryList list = new JournalEntryList();
		// list.printAllClients();
		list.printDuplicateJournal();
		System.out.println("=================================================");
		/*while (list.hasNext()) {
			System.out.println("Client:" + list.next());
		}*/
	}

}
