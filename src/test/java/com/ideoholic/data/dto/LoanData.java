package com.ideoholic.data.dto;

public class LoanData {
	private Integer Id;
	private String nameOfTheMember;
	
	private String loanPrincipalAmount;
	private String loanTerm;
		
	private String loanProductType;
	
	
	private String loanDisburseDate;
	private String loanSubmittedDate;
	private String loanChargeAmount;
	private String loanChargeDueOnDate;
	
	public LoanData(Integer Id,String nameOfTheMember,String loanProductType,
			String loanPrincipalAmount, String loanTerm,String loanChargeAmount, String loanChargeDueOnDate,String loanDisburseDate, String loanSubmittedDate) {
		this.Id=Id;
		this.nameOfTheMember=nameOfTheMember.trim();
		this.loanProductType = loanProductType.trim();
		this.loanPrincipalAmount = loanPrincipalAmount.trim();
		this.loanTerm = loanTerm;
		this.loanChargeAmount = loanChargeAmount;
		this.loanChargeDueOnDate = loanChargeDueOnDate;
		this.loanDisburseDate = loanDisburseDate;
		this.loanSubmittedDate = loanSubmittedDate;
	}
	
		
	public String getLoanProductType() {
		return loanProductType;
	}

	public void setLoanProductType(String loanProductType) {
		this.loanProductType = loanProductType;
	}

	public String getLoanPrincipalAmount() {
		return loanPrincipalAmount;
	}

	public void setLoanPrincipalAmount(String loanPrincipalAmount) {
		this.loanPrincipalAmount = loanPrincipalAmount;
	}

	public String getLoanTerm() {
		return loanTerm;
	}

	public void setLoanTerm(String loanTerm) {
		this.loanTerm = loanTerm;
	}

	public String getLoanChargeAmount() {
		return loanChargeAmount;
	}

	public void setLoanChargeAmount(String loanChargeAmount) {
		this.loanChargeAmount = loanChargeAmount;
	}

	public String getLoanChargeDueOnDate() {
		return loanChargeDueOnDate;
	}

	public void setLoanChargeDueOnDate(String loanChargeDueOnDate) {
		this.loanChargeDueOnDate = loanChargeDueOnDate;
	}

	public String getLoanDisburseDate() {
		return loanDisburseDate;
	}

	public void setLoanDisburseDate(String loanDisburseDate) {
		this.loanDisburseDate = loanDisburseDate;
	}

	public String getLoanSubmittedDate() {
		return loanSubmittedDate;
	}

	public void setLoanSubmittedDate(String loanSubmittedDate) {
		this.loanSubmittedDate = loanSubmittedDate;
	}


	public String getNameOfTheMember() {
		return nameOfTheMember;
	}


	public void setNameOfTheMember(String nameOfTheMember) {
		this.nameOfTheMember = nameOfTheMember;
	}


	public Integer getId() {
		return Id;
	}


	public void setId(Integer id) {
		Id = id;
	}

	@Override
	public boolean equals(Object o) {

		// null check
		if (o == null) {
			return false;
		}

		// this instance check
		if (this == o) {
			return true;
		}

		// instanceof Check and actual value check
		if (o instanceof LoanData) {
			LoanData otherLoan = (LoanData) o;
			// Check fullname for declaring the Loans to
			// be equal
			if (this.getNameOfTheMember().equalsIgnoreCase(otherLoan.getNameOfTheMember()))
				return true;
		}

		return false;
	}
	
}
