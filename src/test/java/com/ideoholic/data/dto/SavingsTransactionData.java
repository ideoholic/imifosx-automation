package com.ideoholic.data.dto;

/**
 * This class is to contain client data needed for client related operations
 * 
 * @author Ideoholic
 *
 */
public class SavingsTransactionData {
	
	private String clientFullName;
	private String savingsAccountNumber;
	private String transactionDate;
	private String transactionAmount;
	private String transactionType;
	private Integer id;
	

	public SavingsTransactionData(Integer id, String clientFullName, String savingsAccountNumber, String transactionDate,
			String transactionAmount, String transactionType) {
		super();
		this.id = id;
		this.clientFullName = clientFullName;
		this.savingsAccountNumber = savingsAccountNumber;
		this.transactionDate = transactionDate;
		this.transactionAmount = transactionAmount;
		this.transactionType = transactionType;
	}

	

	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getClientFullName() {
		return clientFullName;
	}



	public void setClientFullName(String clientFullName) {
		this.clientFullName = clientFullName;
	}



	public String getSavingsAccountNumber() {
		return savingsAccountNumber;
	}



	public void setSavingsAccountNumber(String savingsAccountNumber) {
		this.savingsAccountNumber = savingsAccountNumber;
	}



	public String getTransactionDate() {
		return transactionDate;
	}



	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}



	public String getTransactionAmount() {
		return transactionAmount;
	}



	public void setTransactionAmount(String transactionAmount) {
		this.transactionAmount = transactionAmount;
	}



	public String getTransactionType() {
		return transactionType;
	}



	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

}
