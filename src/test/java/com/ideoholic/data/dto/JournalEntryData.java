package com.ideoholic.data.dto;

public class JournalEntryData {
	private Integer id;
	private String journalDebitAccountName;
	private String journalDebitAmount;
	private String journalCreditAccountName;
	private String journalCreditAmount;
	private String journalTransactionDate;
	private String journalComments;
	
	public JournalEntryData(Integer id,String journalDebitAccountName, String journalDebitAmount, String journalCreditAccountName,
			String journalCreditAmount, String journalTransactionDate, String journalComments) {
		this.id = id;
		this.journalDebitAccountName = journalDebitAccountName.trim();
		this.journalDebitAmount = journalDebitAmount.trim();
		this.journalCreditAccountName = journalCreditAccountName.trim();
		this.journalCreditAmount = journalCreditAmount.trim();
		this.journalTransactionDate = journalTransactionDate.trim();
		this.journalComments = journalComments.trim();
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}
	public String getJournalDebitAccountName() {
		return journalDebitAccountName;
	}

	public void setJournalDebitAccountName(String journalDebitAccountName) {
		this.journalDebitAccountName = journalDebitAccountName;
	}

	public String getJournalDebitAmount() {
		return journalDebitAmount;
	}

	public void setJournalDebitAmount(String journalDebitAmount) {
		this.journalDebitAmount = journalDebitAmount;
	}

	public String getJournalCreditAccountName() {
		return journalCreditAccountName;
	}

	public void setJournalCreditAccountName(String journalCreditAccountName) {
		this.journalCreditAccountName = journalCreditAccountName;
	}

	public String getJournalCreditAmount() {
		return journalCreditAmount;
	}

	public void setJournalCreditAmount(String journalCreditAmount) {
		this.journalCreditAmount = journalCreditAmount;
	}

	public String getJournalTransactionDate() {
		return journalTransactionDate;
	}

	public void setJournalTransactionDate(String journalTransactionDate) {
		this.journalTransactionDate = journalTransactionDate;
	}

	public String getJournalComments() {
		return journalComments;
	}

	public void setJournalComments(String journalComments) {
		this.journalComments = journalComments;
	}
	
	
}
