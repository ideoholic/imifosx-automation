package com.ideoholic.data.dto;

/**
 * This class is to contain client data needed for client related operations
 * 
 * @author springboard
 *
 */
public class ShareClientData {
	private String fullName;
	private String gender;
	private String dateOfBirth;
	private String totalShares;
	private Integer id;

	public ShareClientData(Integer id, String totalShares, String fullName, String gender, String dateOfBirth) {
		this.id = id;
		this.totalShares = totalShares;
		this.fullName = fullName.trim();
		this.gender = gender.trim();
		this.dateOfBirth = dateOfBirth.trim();
	}



	public String getTotalShares() {
		return totalShares;
	}



	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public void setTotalShares(String totalShares) {
		this.totalShares = totalShares;
	}



	public String getFullName() {
		return fullName;
	}

	public String getGender() {
		return gender;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Total Shares: " + getTotalShares());
		sb.append("\t");
		sb.append("Name: " + getFullName());
		sb.append("\t");
		sb.append("Gender: " + getGender());
		sb.append("\t");
		sb.append("Date of Birth: " + getDateOfBirth());
		return sb.toString();
	}

	// The method does override or implement a method declared in a supertype.
	@Override
	public boolean equals(Object o) {

		// null check
		if (o == null) {
			return false;
		}

		// this instance check
		if (this == o) {
			return true;
		}

		// instanceof Check and actual value check
		if (o instanceof ShareClientData) {
			ShareClientData otherClient = (ShareClientData) o;
			// Check fullname, gender and dateofbirth equality for declaring the clients to
			// be equal
			if (this.getFullName().equalsIgnoreCase(otherClient.getFullName())
					&& this.getGender().equalsIgnoreCase(otherClient.getGender())
					&& this.getDateOfBirth().equals(otherClient.getDateOfBirth()))
				return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		int result = 0;
		result = (int) (id / 11);
		return result;
	}

}
