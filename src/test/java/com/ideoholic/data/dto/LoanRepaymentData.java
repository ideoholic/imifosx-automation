package com.ideoholic.data.dto;

public class LoanRepaymentData {
	private Integer Id;
	private String nameOfTheMemberForLoanTransaction;
	private String loanAccountNumber;
	private String loanTransactionDateForRepayment;
	private String loanTransactionAmountForRepayment;
	
	public LoanRepaymentData(Integer Id,String nameOfTheMemberForLoanTransaction, String loanAccountNumber,String loanTransactionDateForRepayment,
			String loanTransactionAmountForRepayment) {
		this.Id=Id;
		this.nameOfTheMemberForLoanTransaction = nameOfTheMemberForLoanTransaction.trim();
		this.loanAccountNumber = loanAccountNumber.trim();
		this.loanTransactionDateForRepayment = loanTransactionDateForRepayment.trim();
		this.loanTransactionAmountForRepayment = loanTransactionAmountForRepayment.trim();
	}

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getNameOfTheMemberForLoanTransaction() {
		return nameOfTheMemberForLoanTransaction;
	}

	public void setNameOfTheMemberForLoanTransaction(String nameOfTheMemberForLoanTransaction) {
		this.nameOfTheMemberForLoanTransaction = nameOfTheMemberForLoanTransaction;
	}

	public String getLoanAccountNumber() {
		return loanAccountNumber;
	}

	public void setLoanAccountNumber(String loanAccountNumber) {
		this.loanAccountNumber = loanAccountNumber;
	}

	public String getLoanTransactionDateForRepayment() {
		return loanTransactionDateForRepayment;
	}

	public void setLoanTransactionDateForRepayment(String loanTransactionDateForRepayment) {
		this.loanTransactionDateForRepayment = loanTransactionDateForRepayment;
	}

	public String getLoanTransactionAmountForRepayment() {
		return loanTransactionAmountForRepayment;
	}

	public void setLoanTransactionAmountForRepayment(String loanTransactionAmountForRepayment) {
		this.loanTransactionAmountForRepayment = loanTransactionAmountForRepayment;
	}
	
	
}
