package com.ideoholic.data.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@ToString
@SuperBuilder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class ClientData {
	protected int id;
	protected String office;
	protected String fname;
	protected String lname;
	protected String mobileNumber;
	protected String dateOfBirth;
	protected String gender;
	protected String clientType;
	protected String clientClassification;
	protected String externalId;
	protected String creationDate;
	protected String addressType;
	protected String street;
	protected String addressLine1;
	protected String addressLine2;
	protected String city;
	protected String district;
	protected String state;
	protected String country;
	protected String postalCode;
}
