package com.ideoholic.data.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

/**
 * This class is to contain client data needed for client related operations
 * 
 * @author springboard
 *
 */
@Getter
@Setter
@ToString
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ClientDataTlf extends ClientData {
	private String alternativeContact;
	private String education;
	private String maritalStatus;
	private String age;
	private String businessType;
	private String currentJob;
	private String voterID;

}
