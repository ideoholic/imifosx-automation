package com.ideoholic.data.excel.loaders;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import com.ideoholic.data.datalist.ClientsListing;
import com.ideoholic.data.dto.ClientData;
import com.ideoholic.data.dto.ClientDataTlf;
import com.ideoholic.utils.Constants;
import com.ideoholic.utils.excel.ExcelReader;
import com.ideoholic.utils.excel.ExcelReaderFactory;

public class ClientDataLoader implements Constants {

	private static int CLIENT_OFFICE = 0;
	private static int CLIENT_FNAME = 1;
	private static int CLIENT_LNAME = 2;
	private static int CLIENT_MOBILENUMBER = 3;
	private static int CLIENT_DATEOFBIRTH = 4;
	private static int CLIENT_GENDER = 5;
	private static int CLIENT_CLIENTTYPE = 6;
	private static int CLIENT_CLIENTCLASSIFICATION = 7;
	private static int CLIENT_EXTERNALID = 8;
	private static int CLIENT_CREATIONDATE = 9;
	private static int CLIENT_ADDRESSTYPE = 10;
	private static int CLIENT_STREET = 11;
	private static int CLIENT_ADDRESSLINE1 = 12;
	private static int CLIENT_ADDRESSLINE2 = 13;
	private static int CLIENT_CITY = 14;
	private static int CLIENT_DISTRICT = 15;
	private static int CLIENT_STATE = 16;
	private static int CLIENT_COUNTRY = 17;
	private static int CLIENT_POSTALCODE = 18;
	private static int CLIENT_ALTERNATIVECONTACT = 19;
	private static int CLIENT_EDUCATION = 20;
	private static int CLIENT_MARITALSTATUS = 21;
	private static int CLIENT_AGE = 22;
	private static int CLIENT_BUSINESSTYPE = 23;
	private static int CLIENT_CURRENTJOB = 24;
	private static int CLIENT_VOTERID = 25;

	public static ClientsListing getClientList(ClientName client) throws FileNotFoundException, IOException {
		switch (client) {
		case TLF:
			return new ClientsListing(populateTlfDataFromExcel());
		default:
			return null;
		}

	}

	private static HashMap<Integer, ClientData> populateTlfDataFromExcel() throws FileNotFoundException, IOException {
		HashMap<Integer, ClientData> clientMap = new HashMap<Integer, ClientData>();
		int rowCounter = 1;

		ExcelReader reader = ExcelReaderFactory.getExcelReader(MEMBER_DATA_FILE_XLSX, ExcelReaderFactory.TYPE.XLS);
		List<String> singleRow = reader.getNextRowData();
		try {
			while (singleRow != null && !singleRow.isEmpty()) {
				ClientData c = ClientDataTlf.builder().office(singleRow.get(CLIENT_OFFICE))
						.fname(singleRow.get(CLIENT_FNAME)).lname(singleRow.get(CLIENT_LNAME))
						.mobileNumber(singleRow.get(CLIENT_MOBILENUMBER)).dateOfBirth(singleRow.get(CLIENT_DATEOFBIRTH))
						.gender(singleRow.get(CLIENT_GENDER)).clientType(singleRow.get(CLIENT_CLIENTTYPE))
						.clientClassification(singleRow.get(CLIENT_CLIENTCLASSIFICATION))
						.externalId(singleRow.get(CLIENT_EXTERNALID)).creationDate(singleRow.get(CLIENT_CREATIONDATE))
						.addressType(singleRow.get(CLIENT_ADDRESSTYPE)).street(singleRow.get(CLIENT_STREET))
						.addressLine1(singleRow.get(CLIENT_ADDRESSLINE1))
						.addressLine2(singleRow.get(CLIENT_ADDRESSLINE2)).city(singleRow.get(CLIENT_CITY))
						.district(singleRow.get(CLIENT_DISTRICT)).state(singleRow.get(CLIENT_STATE))
						.country(singleRow.get(CLIENT_COUNTRY)).postalCode(singleRow.get(CLIENT_POSTALCODE))
						.alternativeContact(singleRow.get(CLIENT_ALTERNATIVECONTACT))
						.education(singleRow.get(CLIENT_EDUCATION)).maritalStatus(singleRow.get(CLIENT_MARITALSTATUS))
						.age(singleRow.get(CLIENT_AGE)).businessType(singleRow.get(CLIENT_BUSINESSTYPE))
						.currentJob(singleRow.get(CLIENT_CURRENTJOB)).voterID(singleRow.get(CLIENT_VOTERID)).build();
				singleRow = reader.getNextRowData();
				clientMap.put(rowCounter, c);
				rowCounter++;
			}
		} catch (NullPointerException | IndexOutOfBoundsException ex) {
			logger.debug("Excel data for row:" + rowCounter + " is invalid, please check the data");
			throw ex;
		}
		return clientMap;
	}
}
