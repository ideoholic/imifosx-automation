/**
 * 
 */
package com.ideoholic.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ideoholic.data.datalist.SavingsTransactionList;
import com.ideoholic.data.dto.SavingsTransactionData;
import com.ideoholic.pages.ClientDetailsPage;
import com.ideoholic.pages.ClientsListingPage;
import com.ideoholic.pages.HomePage;
import com.ideoholic.pages.LoginPage;
import com.ideoholic.pages.SavingsAccountDetailsPage;
import com.ideoholic.pages.SavingsAccountTransactions;
import com.ideoholic.utils.Constants;
import com.ideoholic.utils.WebElementRetriever;

/**
 * @author Ideoholic
 *
 */
public class SavingsTransaction implements Constants {

	private WebDriver driver;

	@BeforeTest
	public void testSetUp() {
		// Additional options that can be passed to FF
		// It will work even without the profile being passed to FirefoxDriver
		FirefoxOptions options = new FirefoxOptions();
		options.addArguments("--start-maximized");
		options.addArguments("--kiosk");
		options.addArguments("--ignore-certificate-errors");
		options.addArguments("--disable-popup-blocking");
		options.addArguments("disable-infobars");
		options.addArguments("--incognito");

		System.setProperty("webdriver.firefox.bin", FIREFOX);
		System.setProperty("webdriver.gecko.driver", GECKODRIVER);
		driver = new FirefoxDriver(options);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		driver.navigate().to(APPURL);
	}

	@Test(priority = 0)
	public void loginToMifos() {
		// Create a new instance of the login page object
		LoginPage loginPage = new LoginPage(driver);

		logger.trace("SavingsCreation.loginToMifos::setting username");
		// set the username
		loginPage.setUsername(USER);
		logger.trace("SavingsCreation.loginToMifos::setting password");
		// set the password
		loginPage.setPassword(PASSWORD);
		logger.trace("SavingsCreation.loginToMifos::Clicking login");
		// submit the login
		loginPage.submitLogin();

		/*String getTitle = driver.getTitle();
		Assert.assertEquals(getTitle, "Mifos X Member");*/
	}

	@Test(priority = 1)
	public void goToClientListingScreen() {
		HomePage page = new HomePage(driver);
		page.navigateToClientsScreen();
	}

	@Test(priority = 2)
	public void searchForClient() throws FileNotFoundException, IOException {
		ClientsListingPage clientPage = new ClientsListingPage(driver);
		HomePage homePage = new HomePage(driver);
		
		
		SavingsTransactionList list = new SavingsTransactionList();
		// Skipping the first row as that will be the header row
		if (list.hasNext())
			list.next();
		// Iterate over the full data list and find each user
		while (list.hasNext()) {
			SavingsTransactionData savingsTransaction = list.next();
			String fullname = savingsTransaction.getClientFullName();
			logger.trace("SavingsTransaction.searchForClient:: Client Name:" + fullname);
			clientPage.searchForClient(fullname);
			By result = clientPage.validateIfScreenHasAtleastOneClient(fullname);
			logger.trace("SavingsTransaction.searchForClient:: Result for user:" + fullname + "is:" + result);
			boolean proceedToNextClient = false;
			
			if(result!=null) {
				clientPage.clickOnClient(result);
				System.out.println("SAVING ACCOUNT NUMBER "+savingsTransaction.getSavingsAccountNumber());
				searchAccount(savingsTransaction.getSavingsAccountNumber());
				if(Integer.parseInt(savingsTransaction.getTransactionType()) == 1) {
					depositTransaction(savingsTransaction);
				}else if((Integer.parseInt(savingsTransaction.getTransactionType()) == 2)) {
					withdrawTransaction(savingsTransaction);
				}
				
				proceedToNextClient = proceedToNext();
			}
			
			if(proceedToNextClient) {
				homePage.navigateToClientsScreen();
			}
			
			// Assert.assertTrue(result);
		}
	}
	
	private boolean proceedToNext() {
		
		SavingsAccountDetailsPage savingsAccountDetailsPage = new SavingsAccountDetailsPage(driver);
		return savingsAccountDetailsPage.waitForPageToShowAccountDetails();
	}

	private void withdrawTransaction(SavingsTransactionData savingsTransaction) {
		
		SavingsAccountDetailsPage savingsAccountDetailsPage = new SavingsAccountDetailsPage(driver);
		savingsAccountDetailsPage.clickOnWithdraw();
		
		SavingsAccountTransactions transactionsSavingsAccount = new SavingsAccountTransactions(driver);
		transactionsSavingsAccount.doTransactions(savingsTransaction.getTransactionDate(),savingsTransaction.getTransactionAmount());
		
	}

	private void depositTransaction(SavingsTransactionData savingsTransaction) {
		
		SavingsAccountDetailsPage savingsAccountDetailsPage = new SavingsAccountDetailsPage(driver);
		savingsAccountDetailsPage.clickOnDeposit();
		
		SavingsAccountTransactions transactionsSavingsAccount = new SavingsAccountTransactions(driver);
		transactionsSavingsAccount.doTransactions(savingsTransaction.getTransactionDate(),savingsTransaction.getTransactionAmount());
		
	}

	private void searchAccount(String savingsAccountNumber) {
		
		WebElementRetriever webElementRetriever = new WebElementRetriever(driver);
		ClientDetailsPage clientsPage = new ClientDetailsPage(driver);
		clientsPage.waitForClientDetailsPageToLoad();
		WebElement tableElement = webElementRetriever.getTableElement("Saving Account", savingsAccountNumber);
		tableElement.click();
		
	}
	
	
	
	@AfterTest
	public void tearDown() {
		// driver.quit();
	}

}
