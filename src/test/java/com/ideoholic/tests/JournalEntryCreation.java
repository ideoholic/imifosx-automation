package com.ideoholic.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ideoholic.data.datalist.JournalEntryList;
import com.ideoholic.data.datalist.LoanList;
import com.ideoholic.data.dto.JournalEntryData;
import com.ideoholic.data.dto.LoanData;
import com.ideoholic.pages.AccountingPage;
import com.ideoholic.pages.AddJournalEntryPage;
import com.ideoholic.pages.ClientInformationPage;
import com.ideoholic.pages.ClientsListingPage;
import com.ideoholic.pages.JournalEntryPage;
import com.ideoholic.pages.LoanProductCreationPage;
import com.ideoholic.pages.LoanProductSelectionPage;
import com.ideoholic.pages.LoginPage;
import com.ideoholic.pages.TempChargePage;
import com.ideoholic.utils.Constants;

public class JournalEntryCreation implements Constants {

	private WebDriver driver;

	@BeforeTest
	public void testSetUp() {
		// Additional options that can be passed to FF
		// It will work even without the profile being passed to FirefoxDriver
		FirefoxOptions options = new FirefoxOptions();
		options.addArguments("--start-maximized");
		options.addArguments("--kiosk");
		options.addArguments("--ignore-certificate-errors");
		options.addArguments("--disable-popup-blocking");
		options.addArguments("disable-infobars");
		options.addArguments("--incognito");

		System.setProperty("webdriver.firefox.bin", FIREFOX);
		System.setProperty("webdriver.gecko.driver", GECKODRIVER);
		driver = new FirefoxDriver(options);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.navigate().to(APPURL);
	}

	@Test(priority = 0)
	public void loginToMifos() {
		// Create a new instance of the login page object
		LoginPage loginPage = new LoginPage(driver);

		System.out.println("LoanRepayment.loginToMifos::setting username");
		// set the username
		loginPage.setUsername("mifos");
		System.out.println("LoanRepayment.loginToMifos::setting password");
		// set the password
		loginPage.setPassword("password");
		System.out.println("LoanRepayment.loginToMifos::Clicking login");
		// su bmit the login
		loginPage.submitLogin();

		String getTitle = driver.getTitle();
		Assert.assertEquals(getTitle, "Mifos X Member");
	}


	@Test(priority = 1)
	public void createJournalEntry() throws FileNotFoundException, IOException {
		JournalEntryList list = new JournalEntryList();
		// Skipping the first row as that will be the header row
		if (list.hasNext())
			list.next();
		// Iterate over the full data list and find each user
		while (list.hasNext()) {
			goToAccounting();
			JournalEntryData journal = list.next();
			String journalDebitAccountName = journal.getJournalDebitAccountName();
			String journalDebitAmount = journal.getJournalDebitAmount();
			String journalCreditAccountName = journal.getJournalCreditAccountName();
			String journalCreditAmount = journal.getJournalCreditAmount();
			String journalTransactionDate = journal.getJournalTransactionDate();
			String journalComments = journal.getJournalComments();
			System.out.println("CREATING JOURNAL ENTRY FOR VALUES" + "journalDebitAccountName == "
					+ journalDebitAccountName + "journalDebitAmount == " + journalDebitAmount
					+ "journalCreditAccountName == " + journalCreditAccountName +"journalCreditAmount == "+ journalCreditAmount
					+ "journalTransactionDate == "+journalTransactionDate +"journalComments == "+ journalComments);
			AddJournalEntryPage pageForJournalEntry = new AddJournalEntryPage(driver);
			pageForJournalEntry.insertIntoJournalEntry(journalDebitAccountName, journalDebitAmount,
					journalCreditAccountName, journalCreditAmount, journalTransactionDate, journalComments);

		}
	}
	

	public void goToAccounting() {

		AccountingPage pageForAccounting = new AccountingPage(driver);
		pageForAccounting.goToAccountingPage();
		JournalEntryPage pageForJournalEntry = new JournalEntryPage(driver);
		pageForJournalEntry.navigateToAddJournalEntryScreen();

	}
}
