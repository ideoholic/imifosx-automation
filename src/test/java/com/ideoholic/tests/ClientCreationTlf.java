package com.ideoholic.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ideoholic.data.datalist.ClientsListing;
import com.ideoholic.data.dto.ClientDataTlf;
import com.ideoholic.data.excel.loaders.ClientDataLoader;
import com.ideoholic.data.excel.loaders.ClientName;
import com.ideoholic.pages.ClientInformationPage;
import com.ideoholic.pages.ClientsListingPage;
import com.ideoholic.pages.LoginPage;
import com.ideoholic.utils.Constants;

public class ClientCreationTlf implements Constants {

	private WebDriver driver;
	private ClientsListing list;

	@BeforeTest
	public void testSetUp() throws FileNotFoundException, IOException {
		list = ClientDataLoader.getClientList(ClientName.TLF);
	}
	
	@Test(priority = 0)
	public void launchBrowser() {
		// Additional options that can be passed to FF
		// It will work even without the profile being passed to FirefoxDriver
		FirefoxOptions options = new FirefoxOptions();
		// options.addArguments("--kiosk");
		options.addArguments("--ignore-certificate-errors");
		options.addArguments("--disable-popup-blocking");
		options.addArguments("disable-infobars");
		options.addArguments("--incognito");

		System.setProperty("webdriver.firefox.bin", FIREFOX);
		System.setProperty("webdriver.gecko.driver", GECKODRIVER);
		driver = new FirefoxDriver(options);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.navigate().to(APPURL);
	}

	@Test(priority = 1)
	public void loginToMifos() {
		// Create a new instance of the login page object
		LoginPage loginPage = new LoginPage(driver);

		logger.debug("ClientCreationTlf.loginToMifos::setting username");
		// set the username
		loginPage.setUsername(USER);
		logger.debug("ClientCreationTlf.loginToMifos::setting password");
		// set the password
		loginPage.setPassword(PASSWORD);
		logger.debug("ClientCreationTlf.loginToMifos::Clicking login");
		// submit the login
		loginPage.submitLogin();

		String getTitle = driver.getTitle();
		// Assert.assertEquals(getTitle, "Mifos X Client");
	}

	@Test(priority = 2)
	public void createClient() {
		ClientsListingPage clientPage = new ClientsListingPage(driver);
		// Skipping the first row as that will be the header row
		if (list.hasNext())
			list.next();
		// Iterate over the full data list and find each user
		while (list.hasNext()) {
			goToClientPage();
			ClientDataTlf tlfData = (ClientDataTlf) list.next();
			By some = By.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div[2]/div/div[2]/a[1]");
			driver.findElement(some).sendKeys(tlfData.getFname());
		}
	}

	public void goToClientPage() {
		ClientInformationPage page = new ClientInformationPage(driver);
		page.navigateToClientsScreen();
	}

	@AfterTest
	public void tearDown() {
		driver.quit();
	}

}