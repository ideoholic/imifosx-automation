/**
 * 
 */
package com.ideoholic.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ideoholic.data.datalist.ShareClientsList;
import com.ideoholic.data.dto.ShareClientData;
import com.ideoholic.pages.ClientDetailsPage;
import com.ideoholic.pages.ClientsListingPage;
import com.ideoholic.pages.HomePage;
import com.ideoholic.pages.LoginPage;
import com.ideoholic.pages.ShareAccountApplicationPage;
import com.ideoholic.pages.ShareAccountDetailsPage;
import com.ideoholic.pages.ShareApprovePage;
import com.ideoholic.utils.Constants;

/**
 * @author Ideoholic
 *
 */
public class ShareCreation implements Constants {

	private WebDriver driver;

	@BeforeTest
	public void testSetUp() {
		// Additional options that can be passed to FF
		// It will work even without the profile being passed to FirefoxDriver
		FirefoxOptions options = new FirefoxOptions();
		options.addArguments("--start-maximized");
		options.addArguments("--kiosk");
		options.addArguments("--ignore-certificate-errors");
		options.addArguments("--disable-popup-blocking");
		options.addArguments("disable-infobars");
		options.addArguments("--incognito");

		//System.setProperty("webdriver.firefox.bin", FIREFOX);
		System.setProperty("webdriver.gecko.driver", GECKODRIVER);
		driver = new FirefoxDriver(options);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.navigate().to(APPURL);
	}

	@Test(priority = 0)
	public void loginToMifos() {
		// Create a new instance of the login page object
		LoginPage loginPage = new LoginPage(driver);

		logger.trace("SavingsCreation.loginToMifos::setting username");
		// set the username
		loginPage.setUsername(USER);
		logger.trace("SavingsCreation.loginToMifos::setting password");
		// set the password
		loginPage.setPassword(PASSWORD);
		logger.trace("SavingsCreation.loginToMifos::Clicking login");
		// submit the login
		loginPage.submitLogin();

		/*String getTitle = driver.getTitle();
		Assert.assertEquals(getTitle, "Mifos X Member");*/
	}

	@Test(priority = 1)
	public void goToClientListingScreen() {
		HomePage page = new HomePage(driver);
		page.navigateToClientsScreen();
	}

	@Test(priority = 2)
	public void searchForClient() throws FileNotFoundException, IOException {
		ClientsListingPage clientPage = new ClientsListingPage(driver);
		HomePage homePage = new HomePage(driver);
		ShareClientsList list = new ShareClientsList();
		// Skipping the first row as that will be the header row
		if (list.hasNext())
			list.next();
		// Iterate over the full data list and find each user
		while (list.hasNext()) {
			ShareClientData client = list.next();
			String fullname = client.getFullName();
			logger.trace("ShareCreation.searchForClient:: Client Name:" + fullname);
			clientPage.searchForClient(fullname);
			By result = clientPage.validateIfScreenHasAtleastOneClient(fullname);
			logger.trace("ShareCreation.searchForClient:: Result for user:" + fullname + "is:" + result);
			boolean proceedToNextClient = false;
			
			if(result!=null) {
				clientPage.clickOnClient(result);
				clientDetailsPage();
				createShare(Integer.parseInt(client.getTotalShares()));
				approveShare();
				proceedToNextClient = checkShareCreation();
			}
			
			if(proceedToNextClient) {
				homePage.navigateToClientsScreen();
			}
			// Assert.assertTrue(result);
		}
	}

	
	private void clientDetailsPage() {
		ClientDetailsPage clientDetailsPage = new ClientDetailsPage(driver);
		clientDetailsPage.clickOnNewShareAccount();
	}
	
	
	private void createShare(Integer noOfShares) {
		ShareAccountApplicationPage shareAccountApplication = new ShareAccountApplicationPage(driver);
		shareAccountApplication.createNewShareAccount(noOfShares);
	}

	private void approveShare() {
		ShareApprovePage shareAccountApprove = new ShareApprovePage(driver);
		shareAccountApprove.approveNewShare();
		shareAccountApprove.approveShare();
		shareAccountApprove.activateShare();
	}

	private boolean checkShareCreation() {
		ShareAccountDetailsPage shareAccountDetailsPage = new ShareAccountDetailsPage(driver);
		return shareAccountDetailsPage.waitForPageToShowShareAccountDetails();
	}
	
	@AfterTest
	public void tearDown() {
		// driver.quit();
	}

}
