package com.ideoholic.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.ideoholic.data.datalist.LoanList;
import com.ideoholic.data.dto.LoanData;
import com.ideoholic.pages.ClientInformationPage;
import com.ideoholic.pages.ClientsListingPage;
import com.ideoholic.pages.LoanProductCreationPage;
import com.ideoholic.pages.LoanProductSelectionPage;
import com.ideoholic.pages.LoginPage;
import com.ideoholic.pages.TempChargePage;
import com.ideoholic.utils.Constants;

public class LoanCreation implements Constants {

	private WebDriver driver;

	@BeforeTest
	public void testSetUp() {
		// Additional options that can be passed to FF
		// It will work even without the profile being passed to FirefoxDriver
		FirefoxOptions options = new FirefoxOptions();
		options.addArguments("--start-maximized");
		options.addArguments("--kiosk");
		options.addArguments("--ignore-certificate-errors");
		options.addArguments("--disable-popup-blocking");
		options.addArguments("disable-infobars");
		options.addArguments("--incognito");

		System.setProperty("webdriver.firefox.bin", FIREFOX);
		System.setProperty("webdriver.gecko.driver", GECKODRIVER);
		driver = new FirefoxDriver(options);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.navigate().to(APPURL);
	}

	@Test(priority = 0)
	public void loginToMifos() {
		// Create a new instance of the login page object
		LoginPage loginPage = new LoginPage(driver);

		System.out.println("LoanCreation.loginToMifos::setting username");
		// set the username
		loginPage.setUsername(USER);
		System.out.println("LoanCreation.loginToMifos::setting password");
		// set the password
		loginPage.setPassword(PASSWORD);
		System.out.println("LoanCreation.loginToMifos::Clicking login");
		// submit the login
		loginPage.submitLogin();

		String getTitle = driver.getTitle();
		Assert.assertEquals(getTitle, "Mifos X Member");
	}

	@Test(priority = 1)
	public void goToClientPage() {
		ClientInformationPage page = new ClientInformationPage(driver);
		page.navigateToClientsScreen();
	}

	@Test(priority = 2)
	@Parameters({ "typeOfLoanProduct" })
	public void createLoan(int typeOfLoanProduct) throws FileNotFoundException, IOException {
		ClientsListingPage clientPage = new ClientsListingPage(driver);
		LoanList list = new LoanList(typeOfLoanProduct);
		// Skipping the first row as that will be the header row
		if (list.hasNext())
			list.next();
		// Iterate over the full data list and find each user
		while (list.hasNext()) {
			LoanData loan = list.next();
			String fullname = loan.getNameOfTheMember();
			clientPage.searchForClient(fullname);
			By result = clientPage.validateIfScreenHasAtleastOneClient(fullname);
			System.out.println("LoanCreation.searchForClient:: Result for user:" + fullname + "is:" + result);
			// Assert.assertTrue(result);

			ClientInformationPage page = new ClientInformationPage(driver);
			page.navigateToClientsInformationScreen();

			LoanProductSelectionPage loanProductSelection = new LoanProductSelectionPage(driver);
			loanProductSelection.navigateLoanCreationScreen(typeOfLoanProduct);
			
			String principalAmount = loan.getLoanPrincipalAmount();
			String loanTerm = loan.getLoanTerm();
			String repaidEvery = loan.getLoanTerm();
			String loanDisburseDate= loan.getLoanDisburseDate();

			DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
			Date loanChargeDate = null;
			
			try {
				loanChargeDate = dateFormat.parse(loanDisburseDate);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
			String loanSubmittedDate= loan.getLoanSubmittedDate();
			LoanProductCreationPage loanProductCreation = new LoanProductCreationPage(driver);
			loanProductCreation.insertIntoLoanCreationFields(loanDisburseDate,loanSubmittedDate, principalAmount, loanTerm, repaidEvery);
			
			TempChargePage loanChargePage = new TempChargePage(driver);
			loanChargePage.goToChargeSelection();
			loanChargePage.selectLoanCharge(loan.getLoanChargeAmount(),loanChargeDate);
			logger.trace("LOAN CREATION COMPLETED OF CLIENT ::--"+fullname+"--FOR PRODUCT::--"+typeOfLoanProduct);
			goToClientPage();

		}
	}


	@AfterTest
	public void tearDown() {
		// driver.quit();
	}

}