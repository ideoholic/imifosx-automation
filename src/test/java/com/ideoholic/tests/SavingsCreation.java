/**
 * 
 */
package com.ideoholic.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.ideoholic.data.datalist.ClientsList;
import com.ideoholic.data.dto.ClientData;
import com.ideoholic.pages.ClientDetailsPage;
import com.ideoholic.pages.ClientsListingPage;
import com.ideoholic.pages.HomePage;
import com.ideoholic.pages.LoginPage;
import com.ideoholic.pages.SavingApplicationPage;
import com.ideoholic.pages.SavingApprovePage;
import com.ideoholic.utils.Constants;

/**
 * @author Ideoholic
 *
 */
public class SavingsCreation implements Constants {

	private WebDriver driver;

	@BeforeTest
	public void testSetUp() {
		// Additional options that can be passed to FF
		// It will work even without the profile being passed to FirefoxDriver
		FirefoxOptions options = new FirefoxOptions();
		options.addArguments("--start-maximized");
		options.addArguments("--kiosk");
		options.addArguments("--ignore-certificate-errors");
		options.addArguments("--disable-popup-blocking");
		options.addArguments("disable-infobars");
		options.addArguments("--incognito");

		//System.setProperty("webdriver.firefox.bin", FIREFOX);
		System.setProperty("webdriver.gecko.driver", GECKODRIVER);
		driver = new FirefoxDriver(options);
		driver.manage().timeouts().implicitlyWait(90, TimeUnit.SECONDS);
		driver.navigate().to(APPURL);
	}

	@Test(priority = 0)
	public void loginToMifos() {
		// Create a new instance of the login page object
		LoginPage loginPage = new LoginPage(driver);

		logger.trace("SavingsCreation.loginToMifos::setting username");
		// set the username
		loginPage.setUsername(USER);
		logger.trace("SavingsCreation.loginToMifos::setting password");
		// set the password
		loginPage.setPassword(PASSWORD);
		logger.trace("SavingsCreation.loginToMifos::Clicking login");
		// submit the login
		loginPage.submitLogin();

		/*String getTitle = driver.getTitle();
		Assert.assertEquals(getTitle, "Mifos X Member");*/
	}

	@Test(priority = 1)
	public void goToClientListingScreen() {
		HomePage page = new HomePage(driver);
		page.navigateToClientsScreen();
	}

	@Test(priority = 2)
	public void searchForClient() throws FileNotFoundException, IOException {
		ClientsListingPage clientPage = new ClientsListingPage(driver);
		HomePage homePage = new HomePage(driver);
		ClientsList list = new ClientsList();
		// Skipping the first row as that will be the header row
		if (list.hasNext())
			list.next();
		// Iterate over the full data list and find each user
		while (list.hasNext()) {
			ClientData client = list.next();
			String fullname = "Some Name";
			logger.debug("SavingsCreation.searchForClient:: Client Name:" + fullname);
			clientPage.searchForClient(fullname);
			By clientItem = clientPage.validateIfScreenHasAtleastOneClient(fullname);
			logger.debug("SavingsCreation.searchForClient:: Result for user:" + fullname + "is:" + clientItem);
			boolean proceedToNextClient = false;
			if(clientItem!=null) {
				clientPage.clickOnClient(clientItem);
				clientDetailsPage();
				savingsCreation();
				proceedToNextClient = savingsApprove();
			}
			
			if(proceedToNextClient) {
				homePage.navigateToClientsScreen();
			}
			
			// Assert.assertTrue(result);
		}
	}
	
	private void clientDetailsPage() {
		ClientDetailsPage clientDetailsPage = new ClientDetailsPage(driver);
		clientDetailsPage.clickOnNewSaving();
	}
	
	private void savingsCreation() {
		SavingApplicationPage savingAccountApplication = new SavingApplicationPage(driver);
		savingAccountApplication.createNewSaving();
	}

	private boolean savingsApprove() {
		SavingApprovePage savingAccountApprove = new SavingApprovePage(driver);
		savingAccountApprove.approveNewSaving();
		savingAccountApprove.approveSaving();
		savingAccountApprove.activateSaving();
		return savingAccountApprove.waitForPageToShowAccountDetails();
	}
	
	@AfterTest
	public void tearDown() {
		// driver.quit();
	}

}
