package com.ideoholic.utils;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class AppLogger implements Constants {

    // final static Logger logger = Logger.getLogger(AppLogger.class);
    final static Logger logger = Logger.getRootLogger();

    AppLogger() {
        logger.setLevel(Level.ALL);
    }

    public void trace(String lineToPrint) {
        // System.out.println("" + lineToPrint);
        if (logger.isTraceEnabled()) {
            logger.trace(lineToPrint);
        }
    }

    public void debug(String lineToPrint) {
        // System.out.println("" + lineToPrint);
        if (logger.isDebugEnabled()) {
            logger.debug(lineToPrint);
        }
    }

    public void info(String lineToPrint) {
        if (logger.isInfoEnabled()) {
            logger.debug(lineToPrint);
        }
    }

    public void warn(String lineToPrint) {
        logger.warn(lineToPrint);
    }

    public void error(String lineToPrint) {
        logger.error(lineToPrint);
    }

    public void fatal(String lineToPrint) {
        logger.fatal(lineToPrint);
    }
}
