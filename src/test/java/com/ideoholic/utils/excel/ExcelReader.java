/**
 * 
 */
package com.ideoholic.utils.excel;

import java.util.List;

/**
 * @author springboard
 *
 */
public interface ExcelReader {
	List<String> getNextRowData();
	String getText();
}
