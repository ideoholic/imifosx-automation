/**
 * 
 */
package com.ideoholic.utils.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.odftoolkit.simple.SpreadsheetDocument;

import com.ideoholic.utils.Constants;

/**
 * @author springboard
 *
 */
public class ExcelReaderFactory implements Constants {

	private static ExcelReaderFactory factory = new ExcelReaderFactory();

	public enum TYPE {
		XLS, ODS
	}

	/**
	 * 
	 */
	private ExcelReaderFactory() {
	}

	public static ExcelReader getExcelReader(String fileName, TYPE excelType)
			throws FileNotFoundException, IOException {
		ExcelReader reader = null;
		if (excelType == TYPE.ODS) {
			reader = factory.new ExcelReaderOds(fileName);
		} else if (excelType == TYPE.XLS) {
			reader = factory.new ExcelReaderXls(fileName);
		}
		return reader;
	}

	private class ExcelReaderOds implements ExcelReader, Constants {
		private SpreadsheetDocument spreadSheet;
		private int currentRow;

		public ExcelReaderOds(String fileName) throws FileNotFoundException, IOException {
			spreadSheet = getInputStream(new FileInputStream(fileName));
			currentRow = 0;
		}

		public List<String> getNextRowData() {
			List<String> rowData = new LinkedList<String>();
			org.odftoolkit.simple.table.Table sheet = spreadSheet.getSheetByIndex(DEFAULT_SHEET_NUMBER);
			int rowCount = sheet.getRowCount();
			// Only if the current row is total rows of the sheet
			if (currentRow < rowCount) {
				org.odftoolkit.simple.table.Row row = sheet.getRowByIndex(currentRow++);
				int cellCount = row.getCellCount();
				// System.out.println("ExcelReader.getNextRowData::cell count:" + cellCount);
				for (int x = 0; x < cellCount; x++) {
					// System.out.println("ExcelReader.getNextRowData::Going over the cell:" + x);
					org.odftoolkit.simple.table.Cell cell = row.getCellByIndex(x);
					String value = cell.getStringValue();
					rowData.add(value);
				}
			}
			return rowData;
		}

		/**
		 * @param args public static void main(String[] args) throws IOException {
		 *             SpreadsheetDocument ssDoc = getInputStream(new
		 *             FileInputStream(MEMBER_DATA_FILE)); String fileContent =
		 *             getText(ssDoc); System.out.println("File Contents:" +
		 *             fileContent); }
		 */

		private SpreadsheetDocument getInputStream(InputStream inputStream) throws IOException {
			try {
				return SpreadsheetDocument.loadDocument(inputStream);
			} catch (Exception ex) {
				throw new IOException("The format of the file given is invalid for the file format ODS.", ex);
			}
		}

		public String getText(SpreadsheetDocument spreadsheetDocument) {
			System.out.println("ExcelReader.getText::Starting execution");
			StringBuilder stringBuilder = new StringBuilder("");
			int sheetCount = spreadsheetDocument.getSheetCount();
			System.out.println("ExcelReader.getText::Sheet count:" + sheetCount);
			for (int i = 0; i < sheetCount; i++) {
				System.out.println("ExcelReader.getText::Going over the Sheet:" + i);
				org.odftoolkit.simple.table.Table sheet = spreadsheetDocument.getSheetByIndex(i);
				int rowCount = sheet.getRowCount();
				System.out.println("ExcelReader.getText::Row count:" + rowCount);
				for (int y = 0; y < rowCount; y++) {
					System.out.println("ExcelReader.getText::Going over the row:" + y);
					org.odftoolkit.simple.table.Row row = sheet.getRowByIndex(y);
					int cellCount = row.getCellCount();
					System.out.println("ExcelReader.getText::cell count:" + cellCount);
					for (int x = 0; x < cellCount; x++) {
						System.out.println("ExcelReader.getText::Going over the cell:" + x);
						org.odftoolkit.simple.table.Cell cell = row.getCellByIndex(x);
						String value = cell.getStringValue();
						stringBuilder.append(value);
						stringBuilder.append("\t");
					}
					stringBuilder.append("\n");
					System.out.println("Row:" + y + "=" + stringBuilder.toString());
					stringBuilder.delete(0, stringBuilder.length() - 1);
				}
				stringBuilder.append("\n\n");
			}
			return stringBuilder.toString();
		}

		@Override
		public String getText() {
			return getText(spreadSheet);
		}

	}

	private class ExcelReaderXls implements ExcelReader, Constants {
		private Workbook workbook = null;
		private int currentRow;

		public ExcelReaderXls(String fileName) throws FileNotFoundException, IOException {
			workbook = getInputStream(fileName);
			currentRow = 0;
		}

		private Workbook getInputStream(String fileName) throws EncryptedDocumentException, IOException {
			FileInputStream fileInputStream = new FileInputStream(fileName);
			return WorkbookFactory.create(fileInputStream);
		}

		@Override
		public List<String> getNextRowData() {
			// Create a DataFormatter to format and get each cell's value as String
			DataFormatter dataFormatter = new DataFormatter();

			List<String> rowData = new LinkedList<String>();

			// Getting the Sheet at index zero
			Sheet sheet = workbook.getSheetAt(DEFAULT_SHEET_NUMBER);

			int rowCount = sheet.getLastRowNum();

			if ((rowCount > 0) || (sheet.getPhysicalNumberOfRows() > 0)) {
				rowCount++;
			}
			
			if (currentRow < rowCount) {
				Row row = sheet.getRow(currentRow++);
				row.forEach(cell -> {
					String cellValue = dataFormatter.formatCellValue(cell);
					rowData.add(cellValue);
				});
			}
			return rowData;
		}

		public void printAllSheets() {
			// Use a Java 8 forEach with lambda to iterate over all the sheets and print
			// their names
			System.out.println("Retrieving Sheets using Java 8 forEach with lambda");
			workbook.forEach(sheet -> {
				System.out.println("=> " + sheet.getSheetName());
			});
		}

		public String getText() {
			StringBuffer sb = new StringBuffer();
			// Create a DataFormatter to format and get each cell's value as String
			DataFormatter dataFormatter = new DataFormatter();
			// Getting the Sheet at index zero
			Sheet sheet = workbook.getSheetAt(0);
			System.out.println("\n\nIterating over Rows and Columns using Java 8 forEach with lambda\n");
			sheet.forEach(row -> {
				row.forEach(cell -> {
					String cellValue = dataFormatter.formatCellValue(cell);
					sb.append(cellValue + "\t");
				});
				sb.append("\n");
			});
			return sb.toString();
		}

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws IOException, InvalidFormatException {
		// Creating a Workbook from an Excel file (.xls or .xlsx)
		Workbook workbook = WorkbookFactory.create(new File(MEMBER_DATA_FILE_XLSX));

		// Retrieving the number of sheets in the Workbook
		System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets");

		/*
		 * ============================================================= Iterating over
		 * all the sheets in the workbook (Multiple ways)
		 * =============================================================
		 */

		// 1. You can obtain a sheetIterator and iterate over it
		Iterator<Sheet> sheetIterator = workbook.sheetIterator();
		System.out.println("Retrieving Sheets using Iterator");
		while (sheetIterator.hasNext()) {
			Sheet sheet = sheetIterator.next();
			System.out.println("=> " + sheet.getSheetName());
		}

		// 2. Or you can use a for-each loop
		System.out.println("Retrieving Sheets using for-each loop");
		for (Sheet sheet : workbook) {
			System.out.println("=> " + sheet.getSheetName());
		}

		// 3. Or you can use a Java 8 forEach with lambda
		System.out.println("Retrieving Sheets using Java 8 forEach with lambda");
		workbook.forEach(sheet -> {
			System.out.println("=> " + sheet.getSheetName());
		});

		/*
		 * ================================================================== Iterating
		 * over all the rows and columns in a Sheet (Multiple ways)
		 * ==================================================================
		 */

		// Getting the Sheet at index zero
		Sheet sheet = workbook.getSheetAt(0);

		// Create a DataFormatter to format and get each cell's value as String
		DataFormatter dataFormatter = new DataFormatter();

		// 1. You can obtain a rowIterator and columnIterator and iterate over them
		System.out.println("\n\nIterating over Rows and Columns using Iterator\n");
		Iterator<Row> rowIterator = sheet.rowIterator();
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();

			// Now let's iterate over the columns of the current row
			Iterator<Cell> cellIterator = row.cellIterator();

			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				String cellValue = dataFormatter.formatCellValue(cell);
				System.out.print(cellValue + "\t");
			}
			System.out.println();
		}

		// 2. Or you can use a for-each loop to iterate over the rows and columns
		System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
		for (Row row : sheet) {
			for (Cell cell : row) {
				String cellValue = dataFormatter.formatCellValue(cell);
				System.out.print(cellValue + "\t");
			}
			System.out.println();
		}

		// 3. Or you can use Java 8 forEach loop with lambda
		System.out.println("\n\nIterating over Rows and Columns using Java 8 forEach with lambda\n");
		sheet.forEach(row -> {
			row.forEach(cell -> {
				String cellValue = dataFormatter.formatCellValue(cell);
				System.out.print(cellValue + "\t");
			});
			System.out.println();
		});

		// Closing the workbook
		workbook.close();
	}

}
