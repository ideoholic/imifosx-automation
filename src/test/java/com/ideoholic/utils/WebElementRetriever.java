package com.ideoholic.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WebElementRetriever implements Constants{

	private final WebDriver driver;
	
	String xpathForDiv = "/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/div[2]/div/div[1]/div[2]/div[1]/div[4]/div[";
    String appendForDiv = "]/table[1]/tbody/tr[1]/th[2]";
    String xpathForTable = "/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/div[2]/div/div[1]/div[2]/div[1]/div[4]/div[";
    String appendForTable = "]/table[1]/tbody/tr[";
    
    /**
	 * 
	 */
	public WebElementRetriever(WebDriver driver) {
		this.driver = driver;
	}
    
    public WebElement getTableElement(String tableName, String rowIdentifier) {
    	
    	int divNo = 4;
        while (true) {
          try {
            WebElement div = driver.findElement(By.xpath(xpathForDiv + divNo + appendForDiv));
            String divName = div.getText();

            logger.trace("DIV NAME" + divName);

            if (divName.equalsIgnoreCase(tableName)) {
            	int i=2;
              while(true) {
                WebElement rowElement = driver.findElement(By.xpath(xpathForTable + divNo + appendForTable + i + "]/td[1]"));
                if (rowIdentifier.equalsIgnoreCase(rowElement.getText())) {
                		return rowElement;
                }
                i++;
              }
            } else {
            	 divNo += 1;
            }
           
          } catch (Exception ex) {
        	  logger.trace(""+ex);
            break;
          }
        }
    	
    	return null;
    }
    
}
