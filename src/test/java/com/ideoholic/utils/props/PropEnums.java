/**
 * 
 */
package com.ideoholic.utils.props;

/**
 * Enums required for the properties that are to be accessed from the properties
 * file. Each enum will have a string that defines the value that needs to be
 * read from the file. This string is a constant that will be defined in the
 * .properties file to be looked-up for the value
 *
 */
public enum PropEnums {

	APPURL("application.url"),
	FIREFOXPATH("firefox.path"),
	GEKOPATH("geko.path"),
	DEFAULT_WAIT_TIME("default.wait.time"),
	DATA_FILE_ODS("datafile.ods"),
	DATA_FILE_XLSX("datafile.xlsx"),
	USER("application.user"),
	PASSWORD("application.password"),
	PRODUCT_SAVINGS("product.savings"),
	SAVINGS_DATA_FILE_XLSX("savngsdatafile.xlsx"),
	LOAN_CREATION_DATA_FILE_XLSX("loan.creation.datafile.xlsx"),
	LOAN_TRANSACTION_DATA_FILE_XLSX("loan.transaction.datafile.xlsx"),
	JOURNAL_CREATION_DATA_FILE_XLSX("journalcreationdata.xlsx"),
	INDEX_OF_LOAN_PRODUCT("index.loan.product"),
	LOAN_DISBURSE_DATE("loan.disburse.date"),
	SAVINGS_CREATION_DATE("savings.creation.date"),
	INDEX_OF_LOAN_CHARGE_PRODUCT_SELECTION("index.loan.charge.product"),
	SHARE_CREATION_DATE("share.creation.date"),
	GOLD_LOAN_CREATION_DATA_FILE("gold.loan.creation.datafile.xlsx"),
	MICROFINANCE_LOAN_CREATION_DATA_FILE("microfinance.loan.creation.datafile.xlsx"),
	STAFF_LOAN_CREATION_DATA_FILE("staff.loan.creation.datafile.xlsx"),
	VEHICLE_LOAN_CREATION_DATA_FILE("vehicle.loan.creation.datafile.xlsx");
	private final String propertyName;

	/**
	 * 
	 */
	PropEnums(final String code) {
		this.propertyName = code;
	}
	
    public String getValue() {
        return this.propertyName;
    }

}
