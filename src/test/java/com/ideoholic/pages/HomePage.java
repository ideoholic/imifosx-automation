/**
 * 
 */
package com.ideoholic.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.utils.Constants;

/**
 * @author springboard
 *
 */
public class HomePage implements Constants {

	private final WebDriver driver;
	private final By clientMenu = By.xpath("/html/body/div[2]/div[2]/div[2]/div[1]/nav/div/div[2]/ul[1]/li[1]/a");
	private final By clientMenuItem = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[1]/nav/div/div[2]/ul[1]/li[1]/ul/li[1]/a");

	/**
	 * 
	 */
	public HomePage(WebDriver driver) {
		this.driver = driver;
		waitForPageToLoad();
	}

	/**
	 * Method to make the progress wait unitl the page is actually fully loaded
	 */
	private void waitForPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("HomePage.waitForPageToLoad : Waiting for element");
		// wait.until(ExpectedConditions.elementToBeClickable(clientMenu));
		wait.until(ExpectedConditions.visibilityOfElementLocated(clientMenu));

	}

	public void navigateToClientsScreen() {
		WebElement clients = driver.findElement(clientMenu);
		logger.trace("HomePage.navigateToClientsScreen::Found clients menu:" + clients);
		Actions action = new Actions(driver);

		action.moveToElement(clients).perform();
		clients.click();
		WebElement clientsItem = driver.findElement(clientMenuItem);
		logger.trace("HomePage.navigateToClientsScreen::Found clients sub-item menu:" + clientsItem);
		clientsItem.click();
		driver.manage().timeouts().implicitlyWait(DEFAULT_WAIT_TIME, TimeUnit.SECONDS);
	}

}
