/**
 * 
 */
package com.ideoholic.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.utils.Constants;

/**
 * @author springboard
 *
 */
public class SavingsAccountTransactions implements Constants {

	private final WebDriver driver;
	private final By transactionDate = By.id("transactionDate");
	private final By transactionAmount = By.id("transactionAmount");
	private final By transactionSave = By.id("save");
	
	/**
	 * 
	 */
	public SavingsAccountTransactions(WebDriver driver) {
		this.driver = driver;
		waitForPageToLoad();
	}

	/**
	 * Method to make the progress wait unitl the page is actually fully loaded
	 */
	private void waitForPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("WithdrawSavingsAccount.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(transactionDate));
		logger.trace("WithdrawSavingsAccount.waitForPageToLoad : element visible, returning");
	}
	
	
	public void doTransactions(String dateTransaction, String amountTransaction) {
		
		WebElement transDate = driver.findElement(transactionDate);
		transDate.clear();
		transDate.sendKeys(dateTransaction);
		
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ESCAPE).perform();
		
		WebElement transAmount = driver.findElement(transactionAmount);
		transAmount.clear();
		transAmount.sendKeys(amountTransaction);
		
		WebElement savingTransactionButton = driver.findElement(transactionSave);
		savingTransactionButton.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
}
