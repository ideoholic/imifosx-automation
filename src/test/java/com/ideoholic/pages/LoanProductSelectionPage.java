package com.ideoholic.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.utils.Constants;

public class LoanProductSelectionPage implements Constants{

	private final WebDriver driver;

	private final By clientLoan = By.cssSelector("div.col-md-12:nth-child(1) > span:nth-child(2) > a:nth-child(1)");
	private final By clientLoanProduct = By.id("productId");
	
	public LoanProductSelectionPage(WebDriver driver) {
		this.driver = driver;
		waitForPageToLoad();
	}
	
	private void waitForPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("ClientsListingPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(clientLoan));
		logger.trace("ClientsListingPage.waitForPageToLoad : element visible, returning");
	}

	public void navigateLoanCreationScreen(int indexOfProduct) {
		WebElement newClientLoan = driver.findElement(clientLoan);
		newClientLoan.click();
		WebElement loanProduct = driver.findElement(clientLoanProduct);
		loanProduct.click();
		Select selectLoanProduct = new Select(loanProduct);
		selectLoanProduct.selectByIndex(indexOfProduct);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
	}

}
