package com.ideoholic.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.utils.Constants;
import com.ideoholic.utils.WebElementRetriever;

public class LoanDetailsPage implements Constants {
	private final WebDriver driver;
	
	private final By loanDiv = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/div[2]/div/div[1]/div[2]/div[1]/div[4]/div[4]");
	private final By clientMakeRepayment = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div[3]/div[2]/div/a[5]");
	
	public LoanDetailsPage(WebDriver driver) {
		this.driver = driver;
		waitForPageToLoad();
	}

	private void waitForPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("ClientsListingPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(loanDiv));
		logger.trace("ClientsListingPage.waitForPageToLoad : element visible, returning");
	}
	public void navigateToLoanDetailsPage(String loanAccountNumber) {
		WebElementRetriever retrievedElement = new WebElementRetriever(driver);
		
		WebElement rowElement = retrievedElement.getTableElement("Loan Account", loanAccountNumber);
		rowElement.click();
		
		WebElement MakeRepayment = driver.findElement(clientMakeRepayment);
		MakeRepayment.click();
	}
}
