/**
 * 
 */
package com.ideoholic.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.utils.Constants;

/**
 * @author springboard
 *
 */
public class SavingApprovePage implements Constants {

	private final WebDriver driver;
	private final By selectSavingAccount = By.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div[1]/ul/li[4]");
	private final By approveSaving = By.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div[2]/div[3]/div[2]/div/a[2]");
	private final By savingApprovedDate = By.id("approvedOnDate");
	private final By approveSavingApplication = By.id("save");
	private final By activateSavingAccount = By.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div[2]/div[3]/div[2]/div/a[2]");
	private final By selectActivate = By.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/ul/li[2]");
	private final By savingActivateDate = By.id("activatedOnDate");
	private final By savingDetailsPage = By.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div[2]/div[3]/div[2]/div/a[3]");
	
	/**
	 * 
	 */
	public SavingApprovePage(WebDriver driver) {
		this.driver = driver;
		waitForPageToLoad();
	}

	/**
	 * Method to make the progress wait unitl the page is actually fully loaded
	 */
	private void waitForPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("ClientsListingPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(selectSavingAccount));
		logger.trace("ClientsListingPage.waitForPageToLoad : element visible, returning");
	}
	
	private void waitForPageToLoadActivate() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("SavingApprovePage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(selectActivate));
		logger.trace("SavingApprovePage.waitForPageToLoad : element visible, returning");
	}
	
	private void waitForPageToLoadApprove() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("SavingApprovePage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(approveSaving));
		logger.trace("SavingApprovePage.waitForPageToLoad : element visible, returning");
	}
	
	public boolean waitForPageToShowAccountDetails() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("SavingApprovePage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(savingDetailsPage));
		logger.trace("SavingApprovePage.waitForPageToLoad : element visible, returning");
		String isWithdrawText = driver.findElement(savingDetailsPage).getText();
		logger.trace("******************* "+isWithdrawText);
		if("Withdraw".equalsIgnoreCase(isWithdrawText)) {
			return true;
		}
		return false;
	}
	
	public void waitForPageToShowAccountApproveDate() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("SavingApprovePage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(savingApprovedDate));
		logger.trace("SavingApprovePage.waitForPageToLoad : element visible, returning");
	}
	
	public void waitForPageToShowActivatePage() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("SavingApprovePage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(activateSavingAccount));
		logger.trace("SavingApprovePage.waitForPageToLoad : element visible, returning");
	}
	
	public void approveNewSaving() {
		waitForPageToLoadApprove();
		WebElement approveSavingButton = driver.findElement(approveSaving);
		approveSavingButton.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	public void approveSaving() {
		waitForPageToShowAccountApproveDate();
		WebElement approveSavingDate = driver.findElement(savingApprovedDate);
		approveSavingDate.clear();
		approveSavingDate.sendKeys(SAVINGS_CREATION_DATE);
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ESCAPE).perform();
		 
		WebElement approveSavingApplicationButton = driver.findElement(approveSavingApplication);
		approveSavingApplicationButton.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	public void activateSaving() {
		
		waitForPageToShowActivatePage();

		WebElement approveSavingApplicationButton = driver.findElement(activateSavingAccount);
		approveSavingApplicationButton.click();
		
		waitForPageToLoadActivate();
		
		WebElement approveSavingDate = driver.findElement(savingActivateDate);
		approveSavingDate.clear();
		approveSavingDate.sendKeys(SAVINGS_CREATION_DATE);
		
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ESCAPE).perform();
		
		WebElement activateSavingApplicationButton = driver.findElement(approveSavingApplication);
		activateSavingApplicationButton.click();
		
	}

}
