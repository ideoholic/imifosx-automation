/**
 * 
 */
package com.ideoholic.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.utils.Constants;

/**
 * @author springboard
 *
 */
public class ClientDetailsPage implements Constants {

	private final WebDriver driver;
	private final By clientNewShareAccount = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/div[2]/div/div[1]/div[1]/div/span[4]/a");
	private final By clientNewSaving = By.
			xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/div[2]/div/div[1]/div[1]/div/span[3]/a");
	private final By savingsTable= By.
			xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/div[2]/div/div[1]/div[2]/div[1]/div[4]/div[5]/table[1]");
	/**
	 * 
	 */
	public ClientDetailsPage(WebDriver driver) {
		this.driver = driver;
		waitForPageToLoad();
	}

	/**
	 * Method to make the progress wait unitl the page is actually fully loaded
	 */
	private void waitForPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("ClientsListingPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(savingsTable));
		logger.trace("ClientsListingPage.waitForPageToLoad : element visible, returning");
	}

	public void waitForClientDetailsPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("ClientsListingPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(clientNewShareAccount));
		logger.trace("ClientsListingPage.waitForPageToLoad : element visible, returning");
	}
	
	public void clickOnNewShareAccount() {
		WebElement clientNewShareAccountButton = driver.findElement(clientNewShareAccount);
		clientNewShareAccountButton.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	public void clickOnNewSaving() {
		WebElement clientNewSavingButton = driver.findElement(clientNewSaving);
		clientNewSavingButton.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
}
