/**
 * 
 */
package com.ideoholic.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.utils.Constants;

/**
 * @author springboard
 *
 */
public class ClientsListingPage implements Constants {

	private final WebDriver driver;
	private final By findClentTextBox = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div[2]/div/div[1]/form/div/div[2]/div/input");
	private final By clientSearchButton = By.xpath(
			"/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div[2]/div/div[1]/form/div/div[2]/div/span/button");
	private final By singleClientItem = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div[2]/table/tbody/tr/td[1]");

	/**
	 * 
	 */
	public ClientsListingPage(WebDriver driver) {
		this.driver = driver;
		waitForPageToLoad();
	}

	/**
	 * Method to make the progress wait unitl the page is actually fully loaded
	 */
	private void waitForPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("ClientsListingPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(findClentTextBox));
		logger.trace("ClientsListingPage.waitForPageToLoad : element visible, returning");
	}

	public void searchForClient(String clientName) {
		WebElement findclient = driver.findElement(findClentTextBox);
		logger.trace(
				"ClientsListingPage.waitForPageToLoad : found text box:" + findclient + "entering value:" + clientName);
		findclient.click();
		findclient.clear();
		findclient.sendKeys(clientName);
		WebElement search = driver.findElement(clientSearchButton);
		search.click();
	}

	public By validateIfScreenHasAtleastOneClient(String fullname) {
	        String exceptionMessage = "";
		logger.debug("ClientsListingPage.validateIfScreenHasAtleastOneClient::Passed Name:" + fullname + ":");
		By clientItem = null;
		int i=0;
		
		for(int iCounter = 0; iCounter < 30; iCounter++) {
		    if(i==0) {
		        clientItem = By
                                .xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div[2]/table/tbody/tr/td[1]");   
		    }
		    else {
			clientItem = By
					.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div[2]/table/tbody/tr["+i+"]/td[1]");
		    }
			
                    try {
                        WebElement firstClientFromSearchResult = driver.findElement(clientItem);
                        logger.debug("ClientsListingPage.validateIfScreenHasAtleastOneClient::Element Name:" + firstClientFromSearchResult.getText() + ":");
                        if (fullname.equalsIgnoreCase(firstClientFromSearchResult.getText())) {
                            return clientItem;
                        }
                    } catch (Exception e) {
                        exceptionMessage = e.getMessage();
                        logger.debug("ClientsListingPage.validateIfScreenHasAtleastOneClient::Caught exception:" + exceptionMessage);
                        i = -1;
                    }
        
                    i++;
		}
		
		throw new RuntimeException(exceptionMessage);
	}

	public void clickOnClient(By clientItem) {
		WebElement firstClientFromSearchResult = driver.findElement(clientItem);
		firstClientFromSearchResult.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

}
