package com.ideoholic.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.utils.Constants;

public class AddJournalEntryPage implements Constants {
	private final WebDriver driver;
	private final By currencyField = By.xpath("//*[@id=\"currencyCode\"]");

	private final By journalDebitAccountNameMenu = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/form/fieldset/div[2]/div/div[1]/div");
	private final By journalDebitAccountNameField = By.xpath(
			"/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/form/fieldset/div[2]/div/div[1]/div/div/div/input");
	private final By journalDebitAccountNameClick = By.xpath(
			"/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/form/fieldset/div[2]/div/div[1]/div/div/ul/li[1]");
	private final By journalDebitAmountField = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/form/fieldset/div[2]/div/div[1]/input");
	private final By journalCreditAccountNameMenu = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/form/fieldset/div[2]/div/div[2]/div");
	private final By journalCreditAccountNameClick = By.xpath(
			"/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/form/fieldset/div[2]/div/div[2]/div/div/ul/li[1]");
	private final By journalCreditAccountNameField = By.xpath(
			"/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/form/fieldset/div[2]/div/div[2]/div/div/div/input");
	private final By journalCreditAmountField = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/form/fieldset/div[2]/div/div[2]/input");
	private final By journalTransactionDateField = By.xpath("//*[@id=\"transactionDate\"]");
	private final By journalCommentsField = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/form/fieldset/div[6]/div/textarea");
	private final By journalSubmitButton = By.xpath("//*[@id=\"save\"]");
	private final By journalReverseButton = By.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/div[1]/div[1]/span/div/div/a");

	public AddJournalEntryPage(WebDriver driver) {
		this.driver = driver;
		waitForPageToLoad();
	}

	private void waitForPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("ClientsListingPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(currencyField));
		logger.trace("ClientsListingPage.waitForPageToLoad : element visible, returning");
	}
	private void waitForPageToLoadTheReverseButton() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("ClientsListingPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(journalReverseButton));
		logger.trace("ClientsListingPage.waitForPageToLoad : element visible, returning");
	}

	public void insertIntoJournalEntry(String journalDebitAccountName, String journalDebitAmount,
			String journalCreditAccountName, String journalCreditAmount, String journalTransactionDate,
			String journalComments) {
		// TODO Auto-generated method stub
		// WebElement addingJournalEntry = driver.findElement(currencyField);
		// addingJournalEntry.click();

		WebElement journalDebitAccountMenu = driver.findElement(journalDebitAccountNameMenu);
		journalDebitAccountMenu.click();

		WebElement journalDebitAccount = driver.findElement(journalDebitAccountNameField);
		journalDebitAccount.click();
		journalDebitAccount.clear();
		journalDebitAccount.sendKeys(journalDebitAccountName);

		WebElement journalDebitAccountNameClickSingleItem = driver.findElement(journalDebitAccountNameClick);
		journalDebitAccountNameClickSingleItem.click();

		WebElement journalDebitAmountInput = driver.findElement(journalDebitAmountField);
		journalDebitAmountInput.click();
		journalDebitAmountInput.clear();
		journalDebitAmountInput.sendKeys(journalDebitAmount);

		WebElement journalCreditAccountMenu = driver.findElement(journalCreditAccountNameMenu);
		journalCreditAccountMenu.click();

		WebElement journalCreditAccount = driver.findElement(journalCreditAccountNameField);
		journalCreditAccount.click();
		journalCreditAccount.clear();
		journalCreditAccount.sendKeys(journalCreditAccountName);

		WebElement journalCreditAccountNameClickSingleItem = driver.findElement(journalCreditAccountNameClick);
		journalCreditAccountNameClickSingleItem.click();

		WebElement journalCreditAmountInput = driver.findElement(journalCreditAmountField);
		journalCreditAmountInput.click();
		journalCreditAmountInput.clear();
		journalCreditAmountInput.sendKeys(journalCreditAmount);

		WebElement journalTransactionDateInput = driver.findElement(journalTransactionDateField);
		journalTransactionDateInput.click();
		journalTransactionDateInput.clear();
		journalTransactionDateInput.sendKeys(journalTransactionDate);

		WebElement journalCommentsInput = driver.findElement(journalCommentsField);
		journalCommentsInput.click();
		journalCommentsInput.clear();
		journalCommentsInput.sendKeys(journalComments);

		WebElement journalSubmit = driver.findElement(journalSubmitButton);
		journalSubmit.click();
		waitForPageToLoadTheReverseButton();
	}
}
