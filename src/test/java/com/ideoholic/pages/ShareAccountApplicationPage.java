/**
 * 
 */
package com.ideoholic.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.utils.Constants;

/**
 * @author springboard
 *
 */
public class ShareAccountApplicationPage implements Constants {

	private final WebDriver driver;
	private final By selectShareProduct = By.id("productId");
	private final By submittedOnDate = By.id("submittedOnDate");
	private final By totalNumberOfShares = By.id("requestedShares");
	private final By savingsAccount = By.id("savingsAccountId");
	private final By applicationDate = By.id("applicationdate");
	private final By saveShareAccount = By.id("save");
	private final By shareAccountDetails = By.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/div[2]/div/a[2]");
	/**
	 * 
	 */
	public ShareAccountApplicationPage(WebDriver driver) {
		this.driver = driver;
		waitForPageToLoad();
	}

	/**
	 * Method to make the progress wait unitl the page is actually fully loaded
	 */
	private void waitForPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("ClientsListingPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(selectShareProduct));
		logger.trace("ClientsListingPage.waitForPageToLoad : element visible, returning");
	}
	
	public void createNewShareAccount(Integer noOfShares) {
		
		WebElement selectShareProductButton = driver.findElement(selectShareProduct);
		selectShareProductButton.click();
		Select select = new Select(selectShareProductButton);
		select.selectByIndex(1);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		WebElement enterSubmittedOnDate = driver.findElement(submittedOnDate);
		enterSubmittedOnDate.clear();
		enterSubmittedOnDate.sendKeys(SHARE_CREATION_DATE);
		
		WebElement enterNumberOfShares = driver.findElement(totalNumberOfShares);
		enterNumberOfShares.clear();
		enterNumberOfShares.sendKeys(noOfShares.toString());
		
		WebElement selectSavingsAccount = driver.findElement(savingsAccount);
		selectSavingsAccount.click();
		Select dropdownSavingsAccount = new Select(selectSavingsAccount);
		dropdownSavingsAccount.selectByIndex(1);
		
		WebElement enterapplicationDate = driver.findElement(applicationDate);
		enterapplicationDate.clear();
		enterapplicationDate.sendKeys(SHARE_CREATION_DATE);
		
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ESCAPE).perform();
		
		WebElement saveSharingAccountButton = driver.findElement(saveShareAccount);
		saveSharingAccountButton.click();
		
	}
}
