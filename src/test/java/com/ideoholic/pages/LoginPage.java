package com.ideoholic.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.utils.Constants;

public class LoginPage implements Constants{

	private final WebDriver driver;
	private final By username = By.id("uid");
	private final By password = By.id("pwd");
	private final By login = By.id("login-button");

	public LoginPage(WebDriver driver) {
		this.driver = driver;
		waitForPageToLoad();
	}

	/**
	 * Method to make the progress wait unitl the page is actually fully loaded
	 */
	private void waitForPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("LoginPage.waitForPageToLoad : Waiting by element id");
		wait.until(ExpectedConditions.visibilityOfElementLocated(username));
	}

	public String getUsername() {
		return driver.findElement(username).getAttribute("value");
	}

	public void setUsername(String value) {
		WebElement element = driver.findElement(username);
		element.clear();
		element.sendKeys(value);
	}

	public String getPassword() {
		return driver.findElement(password).getAttribute("value");
	}

	public void setPassword(String value) {
		WebElement element = driver.findElement(password);
		element.clear();
		element.sendKeys(value);
	}

	public void submitLogin() {
		driver.findElement(login).click();
	}
}
