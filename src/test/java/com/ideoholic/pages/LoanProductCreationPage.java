package com.ideoholic.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.utils.Constants;

public class LoanProductCreationPage implements Constants{

	private final WebDriver driver;
	
	private final By clientLoanSubmittedDate = By.xpath("//*[@id=\"submittedOnDate\"]");
	private final By clientLoanDisburseDate = By.xpath("//*[@id=\"expectedDisbursementDate\"]");
	private final By clientLoanprincipalAmount = By.xpath("//*[@id=\"principal\"]");
	private final By clientLoanloanTerm = By.xpath("//*[@id=\"loanTermFrequency\"]");
	private final By clientRepaidEvery = By.xpath("//*[@id=\"numberOfRepayments\"]");
	private final By clientLoanSubmit = By.xpath("//*[@id=\"save\"]");

	private final By clientLoanApprove = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div[3]/div[2]/div/a[2]");
	private final By clientLoanApprovedOnDate = By
			.xpath("//*[@id=\"approvedOnDate\"]");
	private final By clientLoanApprovedAmount = By
			.xpath("//*[@id=\"approvedAmt\"]");
	private final By approveLoanDiv = By.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/form/fieldset/table[1]/tbody/tr/td[1]/legend");
	private final By clientLoanApproveSubmit = By.xpath("//*[@id=\"save\"]");
	private final By clientLoanDisburse = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div[3]/div[2]/div/a[2]");
	private final By clientLoanDisburseSubmit = By.xpath("//*[@id=\"save\"]");
	private final By clientLoanDisburseDateApprove = By.xpath("//*[@id=\"actualDisbursementDate\"]");
	
	public LoanProductCreationPage(WebDriver driver) {
		this.driver = driver;
		waitForPageToLoad();
	}
	
	private void waitForPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("LoanProductCreationPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(clientLoanDisburseDate));
		logger.trace("LoanProductCreationPage.waitForPageToLoad : element visible, returning");
	}
	
	private void waitForViewLoanPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("LoanProductCreationPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(clientLoanApprove));
		logger.trace("LoanProductCreationPage.waitForPageToLoad : element visible, returning");
	}
	
	private void waitForApproveLoanAccountPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("LoanProductCreationPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(clientLoanApprovedOnDate));
		logger.trace("LoanProductCreationPage.waitForPageToLoad : element visible, returning");
	}
	
	private void waitForLoanViewPageForDisbursementToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("LoanProductCreationPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(clientLoanDisburse));
		logger.trace("LoanProductCreationPage.waitForPageToLoad : element visible, returning");
	}
	
	private void waitForLoanDisbursementPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("LoanProductCreationPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(clientLoanDisburseSubmit));
		logger.trace("LoanProductCreationPage.waitForPageToLoad : element visible, returning");
	}
	
	public void insertIntoLoanCreationFields(String loanDisburseDate,String loanSubmittedDate, String principalAmount, String loanTerm, String repaidEvery) {
		WebElement findSubmittedDate = driver.findElement(clientLoanSubmittedDate);
		findSubmittedDate.click();
		findSubmittedDate.clear();
		findSubmittedDate.sendKeys(loanSubmittedDate);
		
		WebElement findDisburseDate = driver.findElement(clientLoanDisburseDate);
		findDisburseDate.click();
		findDisburseDate.clear();
		findDisburseDate.sendKeys(loanDisburseDate);

		WebElement findPrincipalAmount = driver.findElement(clientLoanprincipalAmount);
		findPrincipalAmount.click();
		findPrincipalAmount.clear();
		findPrincipalAmount.sendKeys(principalAmount);

		WebElement findLoanTerm = driver.findElement(clientLoanloanTerm);
		findLoanTerm.click();
		findLoanTerm.clear();
		findLoanTerm.sendKeys(loanTerm);

		WebElement findRepaidEvery = driver.findElement(clientRepaidEvery);
		findRepaidEvery.click();
		findRepaidEvery.clear();
		findRepaidEvery.sendKeys(repaidEvery);

		WebElement submit = driver.findElement(clientLoanSubmit);
		submit.click();
		
		waitForViewLoanPageToLoad();
		
		WebElement approve = driver.findElement(clientLoanApprove);
		approve.click();
		
		waitForApproveLoanAccountPageToLoad();
		
		WebElement approvedDate = driver.findElement(clientLoanApprovedOnDate);
		approvedDate.click();
		approvedDate.clear();
		approvedDate.sendKeys(loanDisburseDate);
		WebElement loanChargeDivClick = driver.findElement(approveLoanDiv);
		loanChargeDivClick.click();
		WebElement approvedAmount = driver.findElement(clientLoanApprovedAmount);
		approvedAmount.click();
		
		WebElement approveSubmit = driver.findElement(clientLoanApproveSubmit);
		approveSubmit.click();
		
		waitForLoanViewPageForDisbursementToLoad();
		
		WebElement Disburse = driver.findElement(clientLoanDisburse);
		Disburse.click();
		
		waitForLoanDisbursementPageToLoad();
		
		WebElement DisburseDate = driver.findElement(clientLoanDisburseDateApprove);
		DisburseDate.click();
		DisburseDate.clear();
		DisburseDate.sendKeys(loanDisburseDate);
		WebElement DisburseSubmit = driver.findElement(clientLoanDisburseSubmit);
		DisburseSubmit.click();
		driver.manage().timeouts().implicitlyWait(DEFAULT_WAIT_TIME, TimeUnit.SECONDS);
	}
}
