package com.ideoholic.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.utils.Constants;

public class LoanRepaymentPage implements Constants {
	private final WebDriver driver;

	private final By clientMakeRepaymentDate = By.xpath("//*[@id=\"transactionDate\"]");
	private final By clientMakeRepaymentAmount = By.xpath("//*[@id=\"transactionAmount\"]");
	private final By clientMakeRepaymentSubmit = By.xpath("//*[@id=\"save\"]");
	private final By clientMakeRepayment = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div[3]/div[2]/div/a[5]");

	public LoanRepaymentPage(WebDriver driver) {
		this.driver = driver;
		waitForPageToLoad();
	}

	private void waitForPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("ClientsListingPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(clientMakeRepaymentDate));
		logger.trace("ClientsListingPage.waitForPageToLoad : element visible, returning");
	}
	
	private void waitForClientMakeRepaymentButtonToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("ClientsListingPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(clientMakeRepayment));
		logger.trace("ClientsListingPage.waitForPageToLoad : element visible, returning");
	}

	public void navigateLoanRepaymentScreen(String repaymentDate,String repaymentAmount) {
		
		WebElement repaymentDateField = driver.findElement(clientMakeRepaymentDate);
		repaymentDateField.click();
		repaymentDateField.clear();
		repaymentDateField.sendKeys(repaymentDate);
		
		WebElement repaymentAmountField = driver.findElement(clientMakeRepaymentAmount);
		repaymentAmountField.clear();
		repaymentAmountField.sendKeys(repaymentAmount);
		
		WebElement repaymentSubmitButton = driver.findElement(clientMakeRepaymentSubmit);
		repaymentSubmitButton.click();
		driver.manage().timeouts().implicitlyWait(DEFAULT_WAIT_TIME, TimeUnit.SECONDS);
		waitForClientMakeRepaymentButtonToLoad();
	}

}
