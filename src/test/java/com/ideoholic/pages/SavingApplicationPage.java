/**
 * 
 */
package com.ideoholic.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.utils.Constants;

/**
 * @author springboard
 *
 */
public class SavingApplicationPage implements Constants {

	private final WebDriver driver;
	private final By selectSavingProduct = By.id("productId");
	private final By submittedOnDate = By.id("submittedOnDate");
	private final By saveSavingApplication = By.id("save");
	private final By nominalInterestRate = By.id("nominalAnnualInterestRate");
	
	/**
	 * 
	 */
	public SavingApplicationPage(WebDriver driver) {
		this.driver = driver;
		waitForPageToLoad();
	}

	/**
	 * Method to make the progress wait unitl the page is actually fully loaded
	 */
	private void waitForPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("ClientsListingPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(selectSavingProduct));
		logger.trace("ClientsListingPage.waitForPageToLoad : element visible, returning");
	}
	
	public void createNewSaving() {
		WebElement selectSavingProductButton = driver.findElement(selectSavingProduct);
		selectSavingProductButton.click();
		Select select = new Select(selectSavingProductButton);
		select.selectByVisibleText(PRODUCT_SAVINGS);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		WebElement enterSubmittedOnDate = driver.findElement(submittedOnDate);
		enterSubmittedOnDate.clear();
		enterSubmittedOnDate.sendKeys(SAVINGS_CREATION_DATE);
		
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ESCAPE).perform();
	
		WebElement saveSharingAccountButton = driver.findElement(saveSavingApplication);
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		wait.until(ExpectedConditions.visibilityOf(saveSharingAccountButton));
		saveSharingAccountButton.click();
		
	}

}
