package com.ideoholic.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.utils.Constants;

public class AccountingPage implements Constants {
	private final WebDriver driver;
	
	private final By accountingMenu = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[1]/nav/div/div[2]/ul[1]/li[2]/a");

	public AccountingPage(WebDriver driver) {
		this.driver = driver;
		waitForPageToLoad();
	}
	
	private void waitForPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("ClientsListingPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(accountingMenu));
		logger.trace("ClientsListingPage.waitForPageToLoad : element visible, returning");
	}

	public void goToAccountingPage() {
		WebElement accountingMenuTab = driver.findElement(accountingMenu);
		accountingMenuTab.click();
	}
}
