package com.ideoholic.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.data.dto.ClientData;
import com.ideoholic.utils.Constants;

public class ClientCreationPage implements Constants {

	private final WebDriver driver;
	private final By office = By.xpath(
			"/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div[2]/div/div[1]/div/form/fieldset/div[1]/div[1]/div[1]/div/a/span");

	public ClientCreationPage(WebDriver driver) {
		this.driver = driver;
		waitForPageToLoad();
	}

	private void waitForPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.debug("ClientInformationPage.waitForPageToLoad : Waiting for element");
		// wait.until(ExpectedConditions.elementToBeClickable(clientMenu));
		wait.until(ExpectedConditions.visibilityOfElementLocated(office));

	}

	public void fillClientScreenData(ClientData clientData) {
		WebElement clients = driver.findElement(office);
		logger.debug("ClientInformationPage.navigateToClientsInformationScreen::Found clients menu:" + clients);
		clients.click();
		By officeId = By.name(clientData.getOffice());
		officeId = officeId != null ? officeId : By.id(clientData.getOffice());
		WebElement office = driver.findElement(officeId);
		Select selectLoanProduct = new Select(office);
		selectLoanProduct.selectByVisibleText(clientData.getOffice());
		driver.manage().timeouts().implicitlyWait(DEFAULT_WAIT_TIME, TimeUnit.SECONDS);
	}
}
