package com.ideoholic.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.utils.Constants;

public class ClientInformationPage implements Constants {

	private final WebDriver driver;
	private final By clientMenu = By.xpath("/html/body/div[2]/div[2]/div[2]/div[1]/nav/div/div[2]/ul[1]/li[1]/a");
	private final By clientMenuItem = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[1]/nav/div/div[2]/ul[1]/li[1]/ul/li[1]/a");
	private final By clientName = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div[2]/table/tbody/tr/td[1]");


	public ClientInformationPage(WebDriver driver) {
		this.driver = driver;
		waitForPageToLoad();
	}

	private void waitForPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		System.out.println("ClientInformationPage.waitForPageToLoad : Waiting for element");
		// wait.until(ExpectedConditions.elementToBeClickable(clientMenu));
		wait.until(ExpectedConditions.visibilityOfElementLocated(clientMenu));

	}

	public void navigateToClientsScreen() {
		WebElement clients = driver.findElement(clientMenu);
		System.out.println("ClientInformationPage.navigateToClientsScreen::Found clients menu:" + clients);
		Actions action = new Actions(driver);

		action.moveToElement(clients).perform();
		clients.click();
		WebElement clientsItem = driver.findElement(clientMenuItem);
		System.out.println("ClientInformationPage.navigateToClientsScreen::Found clients sub-item menu:" + clientsItem);
		action.moveToElement(clientsItem).perform();
		clientsItem.click();
		driver.manage().timeouts().implicitlyWait(DEFAULT_WAIT_TIME, TimeUnit.SECONDS);
	}

	public void navigateToClientsInformationScreen() {
		WebElement clients = driver.findElement(clientMenu);
		System.out.println("ClientInformationPage.navigateToClientsInformationScreen::Found clients menu:" + clients);
		Actions action = new Actions(driver);

		WebElement clientsName = driver.findElement(clientName);
		action.moveToElement(clientsName).perform();
		clientsName.click();
		driver.manage().timeouts().implicitlyWait(DEFAULT_WAIT_TIME, TimeUnit.SECONDS);
	}
}
