/**
 * 
 */
package com.ideoholic.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.utils.Constants;

/**
 * @author springboard
 *
 */
public class ShareAccountDetailsPage implements Constants {

	private final WebDriver driver;
	private final By redeemShare = By.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/div[2]/div/a[2]");
	
	/**
	 * 
	 */
	public ShareAccountDetailsPage(WebDriver driver) {
		this.driver = driver;
	}

	
	public boolean waitForPageToShowShareAccountDetails() {
		

		// Set waiting time
					WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
					logger.trace("SavingApprovePage.waitForPageToLoad : Waiting for element");
					wait.until(ExpectedConditions.visibilityOfElementLocated(redeemShare));
					logger.trace("SavingApprovePage.waitForPageToLoad : element visible, returning");
					String isRedeemShares = driver.findElement(redeemShare).getText();
					logger.trace("Is Redeem Shares "+isRedeemShares);
					if("Redeem Shares".equalsIgnoreCase(isRedeemShares)) {
						return true;
					}
					return false;
	}
	

}
