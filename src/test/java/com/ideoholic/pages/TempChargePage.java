package com.ideoholic.pages;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.utils.Constants;

public class TempChargePage implements Constants {
	private final WebDriver driver;

	private final By clickOnAddLoanCharge = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div[3]/div[2]/div/a[1]");
	private final By chargeSelectionMenu = By.xpath("//*[@id=\"chargeId\"]");
	private final By loanChargeSelection = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/form/fieldset/div[1]/div/select/option["
					+ INDEX_OF_LOAN_CHARGE_PRODUCT_SELECTION + "]");
	private final By loanChargeAmountField = By.xpath("//*[@id=\"amount\"]");
	private final By loanChargeDueOnField = By.xpath("//*[@id=\"dueDate\"]");
	private final By loanChargeSubmitButton = By.xpath("//*[@id=\"save\"]");

	private final By loanChargeDiv = By
			.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/form/fieldset/div[2]");

	public TempChargePage(WebDriver driver) {
		this.driver = driver;
		waitForPageToLoad();
	}

	private void waitForPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("ClientsListingPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(clickOnAddLoanCharge));
		logger.trace("ClientsListingPage.waitForPageToLoad : element visible, returning");
	}

	public void goToChargeSelection() {
		WebElement addLoanCharge = driver.findElement(clickOnAddLoanCharge);
		addLoanCharge.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	public void selectLoanCharge(String loanChargeAmount, Date loanChargeDueOnDate) {
		WebElement loanChargeMenu = driver.findElement(chargeSelectionMenu);
		loanChargeMenu.click();

		WebElement selectLoanCharge = driver.findElement(loanChargeSelection);
		selectLoanCharge.click();

		WebElement loanChargeAmountInput = driver.findElement(loanChargeAmountField);
		loanChargeAmountInput.click();
		loanChargeAmountInput.clear();
		loanChargeAmountInput.sendKeys(loanChargeAmount);

		DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
		Calendar myCal = Calendar.getInstance();
		myCal.setTime(loanChargeDueOnDate);
		myCal.add(Calendar.MONTH, +1);
		Date chargeDate = myCal.getTime();
		String chargeDueOnDate = dateFormat.format(chargeDate);
		WebElement loanChargeDueOnDateInput = driver.findElement(loanChargeDueOnField);
		loanChargeDueOnDateInput.click();
		loanChargeDueOnDateInput.clear();
		loanChargeDueOnDateInput.sendKeys(chargeDueOnDate);

		WebElement loanChargeDivClick = driver.findElement(loanChargeDiv);
		loanChargeDivClick.click();

		WebElement addChargeSubmit = driver.findElement(loanChargeSubmitButton);
		addChargeSubmit.click();

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
}
