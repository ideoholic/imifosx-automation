/**
 * 
 */
package com.ideoholic.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.utils.Constants;

/**
 * @author springboard
 *
 */
public class SavingsAccountDetailsPage implements Constants {

	private final WebDriver driver;
	private final By savingsWithdraw = By.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div[2]/div[3]/div[2]/div/a[3]");
	private final By savingsDeposit = By.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div[2]/div[3]/div[2]/div/a[2]");
	
	/**
	 * 
	 */
	public SavingsAccountDetailsPage(WebDriver driver) {
		this.driver = driver;
		waitForPageToLoad();
	}

	/**
	 * Method to make the progress wait unitl the page is actually fully loaded
	 */
	private void waitForPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("SavingsAccountDetailsPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(savingsWithdraw));
		logger.trace("SavingsAccountDetailsPage.waitForPageToLoad : element visible, returning");
	}
	
	public boolean waitForPageToShowAccountDetails() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("SavingApprovePage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(savingsWithdraw));
		logger.trace("SavingApprovePage.waitForPageToLoad : element visible, returning");
		String isWithdrawText = driver.findElement(savingsWithdraw).getText();
		logger.trace("******************* "+isWithdrawText);
		if("Withdraw".equalsIgnoreCase(isWithdrawText)) {
			return true;
		}
		return false;
	}
	
	public void clickOnDeposit() {
		WebElement selectSavingsDepositButton = driver.findElement(savingsDeposit);
		selectSavingsDepositButton.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
	
	public void clickOnWithdraw() {
		WebElement selectSavingsWithdrawButton = driver.findElement(savingsWithdraw);
		selectSavingsWithdrawButton.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}
}
