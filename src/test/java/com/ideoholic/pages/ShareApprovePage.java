/**
 * 
 */
package com.ideoholic.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ideoholic.utils.Constants;

/**
 * @author springboard
 *
 */
public class ShareApprovePage implements Constants {

	private final WebDriver driver;
	private final By approveShare = By.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/div[2]/div/a[2]");
	private final By shareApprovedDate = By.id("approvedDate");
	private final By approveShareApplication = By.id("save");
	private final By activateShareAccount = By.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/div[2]/div/a[2]");
	private final By shareActivateDate = By.id("activatedDate");
	private final By shareDetailsPage = By.xpath("/html/body/div[2]/div[2]/div[2]/div[3]/div/div/div/div/div[2]/div/a[2]");

	/**
	 * 
	 */
	public ShareApprovePage(WebDriver driver) {
		this.driver = driver;
		waitForPageToLoad();
	}

	/**
	 * Method to make the progress wait unitl the page is actually fully loaded
	 */
	private void waitForPageToLoad() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("ClientsListingPage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(approveShare));
		logger.trace("ClientsListingPage.waitForPageToLoad : element visible, returning");
	}
	
	public void waitForPageToShowApproveDate() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("SavingApprovePage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(shareApprovedDate));
		logger.trace("SavingApprovePage.waitForPageToLoad : element visible, returning");
		
	}
	
	public void waitForPageToShowActivateButton() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("SavingApprovePage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(activateShareAccount));
		logger.trace("SavingApprovePage.waitForPageToLoad : element visible, returning");
		
	}
	
	public void waitForPageToShowActivatedField() {
		// Set waiting time
		WebDriverWait wait = new WebDriverWait(driver, DEFAULT_WAIT_TIME);
		logger.trace("SavingApprovePage.waitForPageToLoad : Waiting for element");
		wait.until(ExpectedConditions.visibilityOfElementLocated(shareActivateDate));
		logger.trace("SavingApprovePage.waitForPageToLoad : element visible, returning");
		
	}
	
	public void approveNewShare() {
		WebElement approveShareButton = driver.findElement(approveShare);
		approveShareButton.click();
	}
	
		
	public void approveShare() {
		
		waitForPageToShowApproveDate();

		WebElement approveShareDate = driver.findElement(shareApprovedDate);
		approveShareDate.clear();
		approveShareDate.sendKeys(SHARE_CREATION_DATE);
		
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ESCAPE).perform();
		
		WebElement activateSavingApplicationButton = driver.findElement(approveShareApplication);
		activateSavingApplicationButton.click();
		
	}
	
	public void activateShare() {
		
		waitForPageToShowActivateButton();
		
		WebElement activateSavingApplicationButton = driver.findElement(activateShareAccount);
		activateSavingApplicationButton.click();
		
		waitForPageToShowActivatedField();
		
		WebElement approveShareDate = driver.findElement(shareActivateDate);
		approveShareDate.clear();
		approveShareDate.sendKeys(SHARE_CREATION_DATE);
		
		Actions action = new Actions(driver);
		action.sendKeys(Keys.ESCAPE).perform();
		
		WebElement activateShare = driver.findElement(approveShareApplication);
		activateShare.click();
		
	}

	

}
